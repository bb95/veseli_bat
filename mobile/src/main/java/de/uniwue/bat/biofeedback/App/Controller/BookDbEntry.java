package de.uniwue.bat.biofeedback.App.Controller;

import java.io.Serializable;

/**
 * Created by BB on 05.03.2018.
 */

public class BookDbEntry implements Serializable {

    private static final String TAG = BookDbEntry.class.getSimpleName();
    public String author;
    public String title;


    /**
     * this creates an entry for the bookDB, it contains the author and title of a book
     * @param author represents the author of a book
     * @param title represents the title of a book
     */
    public BookDbEntry(String author, String title) {
        this.author = author;
        this.title = title;

    }

    public String getAuthor() {
        return author;
    }
    public String getTitle() {
        return title;
    }
}
