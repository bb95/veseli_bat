package de.uniwue.bat.biofeedback.App.UI;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import de.uniwue.bat.biofeedback.App.Controller.Book;
import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.R;

public class RegisterUserDialog extends AppCompatDialogFragment {

    private static final String TAG = RegisterUserDialog.class.getSimpleName();
    private EditText editUsername;
    private Controller controller = Controller.getInstance(getActivity());



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_register_user, null);

        builder.setView(view)

                .setPositiveButton("Speichern", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String username = editUsername.getText().toString();
                        controller.postMsg(Controller.NEW_USER, Controller.USER_TAG, username);


                    }
                });

        editUsername = view.findViewById(R.id.edit_username);


        return builder.create();


    }


}
