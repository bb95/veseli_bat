package de.uniwue.bat.biofeedback.App.Functions;

import android.content.Context;
import android.net.Uri;
import android.os.PowerManager;
import android.util.Log;


import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.Sensor;
import de.uniwue.bat.biofeedback.App.Databases.SensorDB;
import de.uniwue.bat.biofeedback.shared.DataMapKeys;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.Arrays;

public class SensorReceiverService extends WearableListenerService {
    private static final String TAG = SensorReceiverService.class.getSimpleName();
    Context context;


    @Override
    public void onCreate() {
        super.onCreate();

        context = SensorReceiverService.this;


    }

    @Override
    public void onPeerConnected(Node peer) {
        super.onPeerConnected(peer);

        Log.i(TAG, "Connected: " + peer.getDisplayName() + " (" + peer.getId() + ")");

    }

    @Override
    public void onPeerDisconnected(Node peer) {
        super.onPeerDisconnected(peer);

        Log.i(TAG, "Disconnected: " + peer.getDisplayName() + " (" + peer.getId() + ")");
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.d(TAG, "onDataChanged()");

        for (DataEvent dataEvent : dataEvents) {
            if (dataEvent.getType() == DataEvent.TYPE_CHANGED) {

                DataItem dataItem = dataEvent.getDataItem(); //von DataEvent wird das DataItem geholt
                Uri uri = dataItem.getUri();
                String path = uri.getPath();
                Log.d(TAG, "onDataChanged() 2 " + Integer.parseInt(uri.getLastPathSegment()));

                if (path.startsWith("/sensors/")) {
                    unpackSensorData(
                            Integer.parseInt(uri.getLastPathSegment()),
                            DataMapItem.fromDataItem(dataItem).getDataMap() // aus DataItem wird DataMap geholt
                    );
                }
            }
        }
    }

    private void unpackSensorData(int sensorType, DataMap dataMap) {
        float[] values = dataMap.getFloatArray(DataMapKeys.VALUES);
        int accuracy = dataMap.getInt(DataMapKeys.ACCURACY);
        long timestamp = dataMap.getLong(DataMapKeys.TIMESTAMP);

        Log.d(TAG, "Received sensor data " + sensorType + " = " + Arrays.toString(values) + Long.toString(timestamp));

        Sensor sensor = new Sensor(sensorType,values,accuracy,timestamp);
        Controller.getInstance(context).postMsg(Controller.SAVE_SENSOR_DATA, Controller.SAVE_SENSOR_DATA_TAG, sensor);


    }




}
