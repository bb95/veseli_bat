package de.uniwue.bat.biofeedback.App.UI;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.R;
import de.uniwue.bat.biofeedback.shared.ClientPaths;

/**
 * Created by BB on 01.04.2018.
 */

public class ManualRecorderActivity extends AppCompatActivity {

    private static final int CLIENT_CONNECTION_TIMEOUT = 15000;
    private static final String TAG = ManualRecorderActivity.class.getSimpleName();

    private Button onBtn;
    private Button offBtn;
    private ExecutorService executorService;
    private GoogleApiClient googleApiClient;
    private Runnable sensorReceiverRunnable;
    private Thread sensorReceiverThread;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_recorder);

        context = ManualRecorderActivity.this;



        sensorReceiverRunnable = new Runnable() {
            @Override
            public void run() {

                controlMeasurementInBackground(ClientPaths.START_MEASUREMENT);

            }
        };


        this.googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();

        this.executorService = Executors.newCachedThreadPool();





        onBtn = (Button) findViewById(R.id.onBtn);
        offBtn = (Button) findViewById(R.id.offBtn);

        onBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                long startMeasureTimestamp = System.currentTimeMillis();
                Controller.getInstance(context).postMsg(Controller.START_MEASUREMENT, Controller.START_MEASUREMENT_TAG, startMeasureTimestamp);

                sensorReceiverThread = new Thread(sensorReceiverRunnable);
                sensorReceiverThread.start();

            }
        });

        offBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                long stopMeasureTimestamp = System.currentTimeMillis();
                Controller.getInstance(context).postMsg(Controller.STOP_MEASUREMENT, Controller.STOP_MEASUREMENT_TAG, stopMeasureTimestamp);

                stopMeasurement();


            }
        });


    }


    private boolean validateConnection() {
        if (googleApiClient.isConnected()) {
            return true;
        }

        ConnectionResult result = googleApiClient.blockingConnect(CLIENT_CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);

        return result.isSuccess();
    }

    public void stopMeasurement() {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                controlMeasurementInBackground(ClientPaths.STOP_MEASUREMENT);
            }
        });
    }

    private void controlMeasurementInBackground(final String path) {
        if (validateConnection()) {
            List<Node> nodes = Wearable.NodeApi.getConnectedNodes(googleApiClient).await().getNodes();

            Log.d(TAG, "Sending to nodes: " + nodes.size());

            for (Node node : nodes) {
                Log.i(TAG, "add node " + node.getDisplayName());
                //Verbindung müsste eigentlich in Zeile 136 gestartet werden, setResultCallback startet nur einen Log Eintrag
                //der Log Eintrag, der bei setResultCallback gestartet wird, besagt, dass die Message erfolgreich gesendet wurde
                Wearable.MessageApi.sendMessage(googleApiClient, node.getId(), path, null)
                        .setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                            @Override
                            public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                Log.d(TAG, "controlMeasurementInBackground(" + path + "): " + sendMessageResult.getStatus().isSuccess());
                            }
                        });
            }
        } else {
            Log.w(TAG, "No connection possible");
        }
    }
}
