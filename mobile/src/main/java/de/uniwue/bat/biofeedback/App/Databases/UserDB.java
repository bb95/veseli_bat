package de.uniwue.bat.biofeedback.App.Databases;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.provider.BaseColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import de.uniwue.bat.biofeedback.App.Controller.Book;
import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.BookDbEntry;


public class UserDB {

    private static final String TAG = UserDB.class.getSimpleName();
    private UserDbHelper mDbHelper;


    public UserDB(Context context){

        this.mDbHelper = new UserDbHelper(context);
    }


    //Methode für den Create Table Befehl
    static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + UserDbContract.UserDbEntry.TABLE_NAME + " ( " +
                    UserDbContract.UserDbEntry._ID + " INTEGER PRIMARY KEY," +
                    UserDbContract.UserDbEntry.COLUMN_NAME_USER_NAME + " TEXT," +
                    UserDbContract.UserDbEntry.COLUMN_NAME_TIMESTAMP_START + " INTEGER," +
                    UserDbContract.UserDbEntry.COLUMN_NAME_TIMESTAMP_END + " INTEGER)";

    //Methode für den Delete Table Befehl
    static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + UserDbContract.UserDbEntry.TABLE_NAME;



    long saveData(String username, long timestampStart){

        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues dbValues = new ContentValues();
        dbValues.put(UserDbContract.UserDbEntry.COLUMN_NAME_USER_NAME,username);
        dbValues.put(UserDbContract.UserDbEntry.COLUMN_NAME_TIMESTAMP_START, timestampStart);
        //End Timestamp will be updated with method saveEndingTimestamp()
        dbValues.put(UserDbContract.UserDbEntry.COLUMN_NAME_TIMESTAMP_END, timestampStart);


        long id = db.insert(UserDbContract.UserDbEntry.TABLE_NAME, null, dbValues);
        db.close();
        return id;

    }

    void saveEndingTimestamp(long timestampEnd, long id){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        String idAsString = id +"";
        //String bookIDStr = bookID+"";
        String[] whereArgs = new String[] {idAsString};



        ContentValues dbValues = new ContentValues();
        dbValues.put(UserDbContract.UserDbEntry.COLUMN_NAME_TIMESTAMP_END, timestampEnd);

        db.update(UserDbContract.UserDbEntry.TABLE_NAME,dbValues, UserDbContract.UserDbEntry._ID + " = ? ", whereArgs);

        db.close();
    }






    public void exportDB(Activity activity) {

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            Log.d(TAG, "Permission is not granted");

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Controller.REQUEST_PERMISSION);


        } else {


            File exportDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/BioReader");

            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }


            final String date = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.getDefault()).format(new Date());
            File file = new File(exportDir, "userDB_"+date+".csv");

            Log.d(TAG, "Datum und Uhrzeit: "+date);
            try {

                file.createNewFile();

                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                SQLiteDatabase db = mDbHelper.getReadableDatabase();
                Cursor curCSV = db.rawQuery("SELECT * FROM UserDB", null);
                csvWrite.writeNext(curCSV.getColumnNames());



                while (curCSV.moveToNext()) {
                    //Which column you want to export
                    String arrStr[] = {curCSV.getString(0), curCSV.getString(1), curCSV.getString(2), curCSV.getString(3)};
                    csvWrite.writeNext(arrStr);
                }
                csvWrite.close();
                curCSV.close();
            } catch (Exception sqlEx) {

                Log.d(TAG, sqlEx.getMessage(), sqlEx);
            }
        }
    }


    // Definition der Contract Klasse, in der gewisse Parameter wie Tabellenname festgelegt werden
// hier wird die Struktur der Datenbank aufgebaut
    class UserDbContract {
        // To prevent someone from accidentally instantiating the contract class,
        // make the constructor private.
        private UserDbContract() {}

        /* Inner class that defines the table contents */
        public class UserDbEntry implements BaseColumns {
            public static final String TABLE_NAME = "UserDB";
            public static final String COLUMN_NAME_USER_NAME = "Username";
            public static final String COLUMN_NAME_TIMESTAMP_START = "startTimestamp";
            public static final String COLUMN_NAME_TIMESTAMP_END = "endTimestamp";




        }
    }

    //Unterklasse die SQLiteOpenHelper extended. Dient der Organisation. Man erstellt
//ein Objekt der Klasse FeedReaderDBHelper und kann auf dieses Objekt die Methoden
// getWritableDatabase() or getReadableDatabase() aufrufen
    class UserDbHelper extends SQLiteOpenHelper {
        // If you change the database schema, you must increment the database version.
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "User.db";

        public UserDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
        }
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }
}