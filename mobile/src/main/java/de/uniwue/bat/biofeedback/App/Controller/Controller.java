package de.uniwue.bat.biofeedback.App.Controller;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import de.uniwue.bat.biofeedback.App.Databases.DBInterface;


/**
 * Created by BB on 04.03.2018.
 */

public class Controller {

    /**
     * All events, e.g. information from outside of the application, like user input or new
     * measurements, are passed as messages in this application.
     * This class, the AppController defines the possible messages through constants in the
     * following.
     */
    //--------------- Message.WHAT) ----------------------------------//

    public static final int NEW_BOOK = 1000;
    public static final int UPDATE_LIBRARY = 1001;
    public static final int LIBRARY_CONTENT = 1002;
    public static final int GET_BOOK_DATA = 1003;
    public static final int SEND_BOOK_DATA = 1004;

    public static final int GET_SENSOR_DATA = 2000;
    public static final int SEND_SENSOR_DATA = 2001;
    public static final int SAVE_SENSOR_DATA = 2011;

     public static final int START_PAGE = 3000;
    public static final int END_PAGE = 3001;

    public static final int GET_READING_PROGRESS_DATA = 4000;
    public static final int SEND_READING_PROGRESS_DATA = 4001;

    public static final int DELETE_BOOK = 5000;

    public static final int SEND_OFF_SCREEN_EVENT = 6000;
    public static final int SEND_HOME_TAB_EVENT = 6001;
    public static final int SEND_BACKPRESS_EVENT = 6002;

    public static final int EXPORT_DATA = 7000;
    public static final int REQUEST_PERMISSION = 7001;


    public static final int SESSION_DATA = 8002;
    public static final int SEND_SESSION_DATA = 8003;

    public static final int SEND_START_BOOK_EVENT = 9000;

    public static final int GET_SESSIONS = 9001;
    public static final int SEND_SESSIONS = 9002;

    public static final int START_MEASUREMENT = 1011;
    public static final int STOP_MEASUREMENT = 1012;

    public static final int CAMERA_HINT_OK = 1100;
    public static final int NEW_USER = 1200;
    public static final int LOGOUT_USER = 1300;


    //--------------- Bundle Keys -------------------------//


    public static final String SAVE_SENSOR_DATA_TAG ="saveSensorData";
    public static final String USER_TAG = "newUser";
    public static final String LOGOUT_USER_TAG = "logoutUser";
    public static final String START_MEASUREMENT_TAG = "startMeasurement";
    public static final String STOP_MEASUREMENT_TAG = "stopMeasurement";
    public static final String SESSION_DATA_TAG = "sessionData";
    public static final String SEND_SESSION_DATA_TAG = "sendSessionData";
    public static final String GET_SESSIONS_TAG = "getSessions";
    public static final String SEND_SESSIONS_TAG = "sendSessions";
    public static final String SEND_START_BOOK_EVENT_TAG = "sendStartBookEvent";
    public static final String BOOK_TAG = "newBook";
    public static final String LIBRARY_TAG = "authorANDtitle";
    public static final String GET_BOOK_DATA_TAG = "getBookData";
    public static final String SEND_BOOK_DATA_TAG = "sendBookData";
    public static final String SEND_SENSOR_DATA_TAG = "sendSensorData";
    public static final String START_PAGE_TAG = "startPageTimestamp";
    public static final String END_PAGE_TAG = "endPageTimestamp";
    public static final String SEND_LAST_PAGE_TAG = "sendLastPage";
    public static final String SEND_READING_PROGRESS_TAG = "sendReadingProgressDB";
    public static final String DELETE_BOOK_TAG = "deleteBook";
    public static final String SEND_OFF_SCREEN_EVENT_TAG = "sendEvent";
    public static final String SEND_HOME_TAB_EVENT_TAG = "homeOrTabEvent";
    public static final String SEND_BACKPRESS_EVENT_TAG = "backpressEvent";

    //-------------------------Bundle ARGS---------------------------------//

    public static final String BOOK_ID = "bookID";
    public static final String VIDEO_CAPTURE = "videoCapture";
    public static final String READING_SESSION = "readingSession";
    public static final String SAVE_BOOK = "savedBook";
    public static final String BOOK_DATA = "bookData";
    public static final String SELECTED_TEXT = "selectedText";
    public static final String META_DATA_DIALOG = "metaDataDialog";
    public static final String WEARABLE_HINT_DIALOG = "wearableHintDialog";


    //--------------------------------------------------------------------//


    private static final String TAG = Controller.class.getSimpleName();
    private Context context;
    private static Controller thisInstance;
    private List<Handler> observers;
    private ControllerHandler controllerHandler;
    private DBInterface dbInterface;



    /**
     * Constructor is private because the AppController is defined as a singleton.
     */
    private Controller(Context context) {
        thisInstance = this;
        this.context = context;
        this.observers = new LinkedList<>();
        this.controllerHandler = new ControllerHandler();
        this.dbInterface = new DBInterface(context);




    }

    public static Controller getInstance(Context context) {

        if (thisInstance == null) {
            Log.d(TAG, "gerInstance() -> new instance returned, ");
            return new Controller(context);

        } else {
            Log.d(TAG, "gerInstance() -> thisInstance returned ");
            return thisInstance;
        }

    }

    /**
     * Handler which are interested in receiving messages of the AppController, are added to the list
     * of observers with this method.
     */
    public void addObserver(Handler handler) {
        observers.add(handler);
    }

    public void removeObserver(Handler handler) {
        observers.remove(handler);
    }

    /**
     * The message msg is send to every observer with this method.
     *
     * @param msg the message to send
     */
    private void sendToObservers(Message msg) {

        for (Handler observer :
                observers) {
            Message copyMsg = new Message();
            copyMsg.copyFrom(msg);
            observer.sendMessage(copyMsg);
        }
    }

    public void postMsg(int msgWhat, String bundleKey, Serializable bundleValue){
        Log.d(TAG, "Bundlekey "+bundleKey);
        Message msg = new Message();
        msg.what = msgWhat;
        Bundle bundle = new Bundle();
        bundle.putSerializable( bundleKey, bundleValue);
        msg.setData(bundle);
        controllerHandler.sendMessage(msg);
    }

    public Handler getControllerHandler() {
        return controllerHandler;
    }

    /**
     * The ControllerHandler Class is used to receive messages from other components, e.g. the
     * bluetooth component, Service or the FragmentConnectionMenu.
     * If a massage with a known constant is received, a corresponding reaction is triggered.
     */

    private class ControllerHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {

            Log.d(TAG, "handleMessage() with msg code: " + msg.what);

            sendToObservers(msg);


        }
    }
}