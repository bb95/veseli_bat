package de.uniwue.bat.biofeedback.App.Databases;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.provider.BaseColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.Event;
import de.uniwue.bat.biofeedback.App.Controller.ReadingSession;


public class BookSessionsDB {

    private static final String TAG = StopReadingEventsDB.class.getSimpleName();
    private BookSessionsDbHelper mDbHelper;


    public BookSessionsDB(Context context){

        this.mDbHelper = new BookSessionsDbHelper(context);
    }


    //Methode für den Create Table Befehl
    static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + BookSessionsDbContract.BookSessionsDbEntry.TABLE_NAME + " ( " +
                    BookSessionsDbContract.BookSessionsDbEntry._ID + " INTEGER PRIMARY KEY," +
                    BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_BOOK_UUID + " TEXT," +
                    BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_START_TIMESTAMP + " INTEGER," +
                    BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_END_TIMESTAMP + " INTEGER,"+
                    BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_ENDING_EVENT + " TEXT)" ;
    ;


    //Methode für den Delete Table Befehl
    static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + BookSessionsDbContract.BookSessionsDbEntry.TABLE_NAME;



   long saveData(Event startBookEvent){
        //mDbHelper = new BookDbHelper(context);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues dbValues = new ContentValues();
        dbValues.put(BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_BOOK_UUID, startBookEvent.getEventName());
        dbValues.put(BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_START_TIMESTAMP, startBookEvent.getTimestamp());
       dbValues.put(BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_END_TIMESTAMP, startBookEvent.getTimestamp());


        long id = db.insert(BookSessionsDbContract.BookSessionsDbEntry.TABLE_NAME, null, dbValues);

        return id;
    }

    void saveEndingTimestamp(Event endBookEvent, long id){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String idAsString = id +"";
        String[] whereArgs = new String[] {idAsString };

        ContentValues dbValues = new ContentValues();
        dbValues.put(BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_END_TIMESTAMP, endBookEvent.getTimestamp());
        dbValues.put(BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_ENDING_EVENT, endBookEvent.getEventName());

        db.update(BookSessionsDbContract.BookSessionsDbEntry.TABLE_NAME,dbValues, BookSessionsDbContract.BookSessionsDbEntry._ID + " = ?", whereArgs);

        db.close();
    }


    ArrayList<ReadingSession> getAllReadingSession(String bookUUID){

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] whereArgs = new String[] {bookUUID};

        Cursor cursor = db.rawQuery("SELECT " + BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_START_TIMESTAMP +"," +
                BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_END_TIMESTAMP + " FROM " +
                BookSessionsDbContract.BookSessionsDbEntry.TABLE_NAME + " WHERE " + BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_BOOK_UUID
                + " = ?" , whereArgs);

        ArrayList<ReadingSession> readingSessions = new ArrayList<ReadingSession>();
        while(cursor.moveToNext()) {
            readingSessions.add(new ReadingSession(bookUUID,cursor.getLong(cursor.getColumnIndexOrThrow(BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_START_TIMESTAMP))
                    , cursor.getLong(cursor.getColumnIndexOrThrow(BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_END_TIMESTAMP))));

        }


        cursor.close();

        return readingSessions;


    }


    public String loadData(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = null;
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder =
                BookSessionsDbContract.BookSessionsDbEntry._ID + " ASC";

        Cursor cursor = db.query(
                BookSessionsDbContract.BookSessionsDbEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        String result = "";
        while(cursor.moveToNext()) {
            result = result + cursor.getInt(
                    cursor.getColumnIndexOrThrow(BookSessionsDbContract.BookSessionsDbEntry._ID)) + " "
                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_BOOK_UUID)) + " "
                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_START_TIMESTAMP)) + " "
                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_ENDING_EVENT)) + " "
                    + cursor.getLong(
                    cursor.getColumnIndexOrThrow(BookSessionsDbContract.BookSessionsDbEntry.COLUMN_NAME_END_TIMESTAMP)) + " "
                    +"\n";

        }

        cursor.close();

        return result;
    }

    public void exportDB(Activity activity) {

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            Log.d(TAG, "Permission is not granted");

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Controller.REQUEST_PERMISSION);


        } else {


            File exportDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/BioReader");

            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }

            final String date = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.getDefault()).format(new Date());
            File file = new File(exportDir, "bookSessions_"+date+".csv");

            try {

                file.createNewFile();

                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                SQLiteDatabase db = mDbHelper.getReadableDatabase();
                Cursor curCSV = db.rawQuery("SELECT * FROM BookSessionsDB", null);
                csvWrite.writeNext(curCSV.getColumnNames());



                while (curCSV.moveToNext()) {
                    //Which column you want to export
                    String arrStr[] = {curCSV.getString(0), curCSV.getString(1),curCSV.getString(2),curCSV.getString(3),
                            curCSV.getString(4)};
                    csvWrite.writeNext(arrStr);
                }
                csvWrite.close();
                curCSV.close();
            } catch (Exception sqlEx) {

                Log.d(TAG, sqlEx.getMessage(), sqlEx);
            }
        }
    }

    // Definition der Contract Klasse, in der gewisse Parameter wie Tabellenname festgelegt werden
// hier wird die Struktur der Datenbank aufgebaut
    class BookSessionsDbContract {
        // To prevent someone from accidentally instantiating the contract class,
        // make the constructor private.
        private BookSessionsDbContract() {}

        /* Inner class that defines the table contents */
        public class BookSessionsDbEntry implements BaseColumns {
            public static final String TABLE_NAME = "BookSessionsDB";
            public static final String COLUMN_NAME_BOOK_UUID= "BookUUID";
            public static final String COLUMN_NAME_START_TIMESTAMP= "TimeStampStart";
            public static final String COLUMN_NAME_ENDING_EVENT= "Event";
            public static final String COLUMN_NAME_END_TIMESTAMP = "TimeStampEnd";

        }
    }

    //Unterklasse die SQLiteOpenHelper extended. Dient der Organisation. Man erstellt
//ein Objekt der Klasse FeedReaderDBHelper und kann auf dieses Objekt die Methoden
// getWritableDatabase() or getReadableDatabase() aufrufen
    class BookSessionsDbHelper extends SQLiteOpenHelper {
        // If you change the database schema, you must increment the database version.
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "BookSession.db";

        public BookSessionsDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
        }
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }
}