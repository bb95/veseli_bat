package de.uniwue.bat.biofeedback.App.Controller;

import java.io.Serializable;

/**
 * Created by BB on 04.03.2018.
 */

public class PagesDbEntry implements Serializable {

    private static final String TAG = PagesDbEntry.class.getSimpleName();

    public String bookUUID; //random-generierte, absolut eindeutige UUID
    public String bookPage;
    public int pagenum;


    /**
     * this creates an entry for the pagesDB that containts the book uuid, the page and pagenum
     * @param bookUUID represents the book uuid
     * @param bookPage represents the text of the page
     * @param pagenum represents the number of the page
     */
    public PagesDbEntry(String bookUUID, String bookPage, int pagenum){
        this.bookUUID = bookUUID;
        this.bookPage = bookPage;
        this.pagenum = pagenum;
    }

}
