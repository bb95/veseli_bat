package de.uniwue.bat.biofeedback.App.UI.BiofeedbackVisualization;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.github.mikephil.charting.data.Entry;


import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.Datapoint;
import de.uniwue.bat.biofeedback.App.Controller.ReadingSession;
import de.uniwue.bat.biofeedback.App.Controller.SensorNames;
import de.uniwue.bat.biofeedback.R;

/**
 * Created by BB on 18.04.2018.
 */

public class ChartListActivity extends AppCompatActivity {

    private final String TAG = ChartListActivity.class.getSimpleName();

    private ListView chartListview;
    private Context context;
    private SensorNames sensorNames;
    private LineDataSet lineDataSet;
    private ChartDataAdapter chartAdapter;
    private ReadingSession readingsSession;
    private ArrayList<ArrayList<Datapoint>> sensorDataSets;
    private TextView noDataText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chartlist);
        chartListview = (ListView) findViewById(R.id.chartListview);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        noDataText = (TextView) findViewById(R.id.noDataText);

        context = ChartListActivity.this;
        sensorNames = new SensorNames();


        readingsSession = (ReadingSession) getIntent().getExtras().getSerializable(Controller.READING_SESSION);

        Controller.getInstance(context).addObserver(ChartListActHandler);
        Controller.getInstance(context).postMsg(Controller.SESSION_DATA, Controller.SESSION_DATA_TAG, readingsSession);

        sensorDataSets = new  ArrayList<ArrayList<Datapoint>>();
        chartAdapter = new ChartDataAdapter(getApplicationContext(), sensorDataSets);
        chartListview.setAdapter(chartAdapter);


    }

    /**
     * adapter that supports LineCharts
     */

    private class ChartDataAdapter extends ArrayAdapter< ArrayList<Datapoint> > {

        public ChartDataAdapter(Context context, ArrayList<ArrayList<Datapoint>> objects) {
            super(context, 0, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Log.d(TAG, "getView() pos "+ position);
            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();

                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_linechart, null);

                holder.chart = (LineChart) convertView.findViewById(R.id.chart_listitem);


                convertView.setTag(holder);

            } else {

                holder = (ViewHolder) convertView.getTag();

            }



            ArrayList<Datapoint> sensorDataSet = getItem(position);
            holder.chart.setNoDataTextColor(getResources().getColor(R.color.Black));
            if(!sensorDataSet.isEmpty()){

                LineData lineData = getLineData(sensorDataSet);

                Log.d(TAG, "ChartData: " +lineData.getDataSets().get(0) + " pos "+ position);
                lineData.getDataSets().isEmpty();


                // apply styling
                lineData.setValueTextColor(Color.BLACK);

                holder.chart.getDescription().setEnabled(false);
                holder.chart.setDrawGridBackground(false);

                // set the marker to the chart
                PagePopup mv = new PagePopup(context, R.layout.pagepopup_layout);
                holder.chart.setMarker(mv);

                XAxis xAxis = holder.chart.getXAxis();
                xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                xAxis.setDrawGridLines(false);

                xAxis.setValueFormatter(new IAxisValueFormatter() {
                    @Override
                    public String getFormattedValue(float value, AxisBase axis) {


                        // convertedTimestamp = originalTimestamp - referenceTimestamp
                        long convertedTimestamp = (long) value;

                        // Retrieve original timestamp
                        // 1523700000 is the referenceTimestamp
                        long originalTimestamp = 1523700000 + convertedTimestamp;
                        long originalTimeStampMillis = TimeUnit.SECONDS.toMillis(originalTimestamp);

                        // Convert timestamp to hour:minute:second
                        //new Date() requires Milliseconds since 'the epoch'
                        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss ");
                        return formatTime.format(new Date(originalTimeStampMillis));

                    }

                });


                YAxis leftAxis = holder.chart.getAxisLeft();
                leftAxis.setLabelCount(5, false);
                leftAxis.setSpaceTop(15f);


                YAxis rightAxis = holder.chart.getAxisRight();
                rightAxis.setEnabled(false);
                rightAxis.setLabelCount(5, false);
                rightAxis.setSpaceTop(15f);


                // set data
                holder.chart.setData(lineData);




                Description d = new Description();
                d.setText(sensorNames.getName(sensorDataSets.get(position).get(0).getId()));
                holder.chart.setDescription(d);



            }

            // do not forget to refresh the chart
            holder.chart.invalidate();

            return convertView;

        }
    }


    private class ViewHolder {
        LineChart chart;


    }



    private LineData getLineData(ArrayList<Datapoint> sensorGraph) {

        //Color Array for setting the color of different channel graphs
        ArrayList<Integer> colors = new ArrayList<Integer>();
        colors.add(getResources().getColor(R.color.Green));
        colors.add(getResources().getColor(R.color.Grey));
        colors.add(getResources().getColor(R.color.Red));
        colors.add(getResources().getColor(R.color.Blue));
        colors.add(getResources().getColor(R.color.Yellow));
        colors.add(getResources().getColor(R.color.Violet));
        colors.add(getResources().getColor(R.color.Brown));
        colors.add(getResources().getColor(R.color.Orange));



        ArrayList<ArrayList<Entry>> entriesList = new ArrayList<ArrayList<Entry>>();
        for(float y : sensorGraph.get(0).getY()){
            entriesList.add(new ArrayList<Entry>());
        }

        Log.d(TAG,"EntiresList "+ entriesList.size());

        ArrayList<Integer> pageList = new ArrayList<Integer>();


        for(Datapoint datapoint : sensorGraph){

            pageList.add(datapoint.getPage());

            long convertedTimestamp = (TimeUnit.MILLISECONDS.toSeconds(datapoint.getX()) - 1523700000);

            for(int i=0; i < datapoint.getY().size(); i++){

                entriesList.get(i).add(new Entry(convertedTimestamp, datapoint.getY().get(i), datapoint.getPage()));

            }

        }

        Log.d(TAG,"PagesList "+pageList);

        ArrayList<LineDataSet> lineDataSets = new ArrayList<>();
        for(int i = 0; i < entriesList.size(); i++){
            Log.d(TAG, "EntriesList "+entriesList.size());

            LineDataSet set = new LineDataSet(entriesList.get(i), "channel "+i);
            set.setColor(colors.get(i));
            lineDataSets.add(set);
        }

        ArrayList<ILineDataSet> sets = new ArrayList<ILineDataSet>();



        for (LineDataSet lineDataSet : lineDataSets) {

            lineDataSet.setHighLightColor(Color.rgb(244, 117, 117));
            lineDataSet.setDrawValues(false);
            lineDataSet.setCircleColors(setColorForPage(pageList));
            sets.add(lineDataSet);
        }


        LineData cd = new LineData(sets);



        return cd;


    }


    private ArrayList<Integer> setColorForPage(ArrayList<Integer> pagesList) {


        int lastPage = pagesList.get(0);
        int color = getResources().getColor(R.color.Pink);
        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int i = 0; i < pagesList.size(); i++) {

            int value = pagesList.get(i);

            if (lastPage != value && value != 0 ) {
                lastPage = value;

                if (color == this.getResources().getColor(R.color.Pink)) {

                    color = this.getResources().getColor(R.color.Petrol);
                    Log.d(TAG, "Petrol " + color);

                    colors.add(color);


                } else {
                    color = this.getResources().getColor(R.color.Pink);
                    Log.d(TAG, "Pink " + color);
                    colors.add(color);
                }

            } else {
                if(value == 0){
                    colors.add(this.getResources().getColor(R.color.Black));

                }else {
                    colors.add(color);
                }


            }


        }

        return colors;
    }


    Handler ChartListActHandler = new Handler() {


        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {

                case Controller.SEND_SESSION_DATA:
                    Log.d(TAG, "SendSessionData");
                    Bundle sensorGraphsBndl = msg.getData();
                    sensorDataSets = (ArrayList<ArrayList<Datapoint>>) sensorGraphsBndl.getSerializable(Controller.SEND_SESSION_DATA_TAG);


                    chartAdapter.clear();
                    chartAdapter.addAll(sensorDataSets);
                    chartAdapter.notifyDataSetChanged();

                    break;

            }
        }
    };

}








