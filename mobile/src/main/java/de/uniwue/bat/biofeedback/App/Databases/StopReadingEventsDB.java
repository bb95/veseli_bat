package de.uniwue.bat.biofeedback.App.Databases;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.provider.BaseColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.Event;


public class StopReadingEventsDB {

    private static final String TAG = StopReadingEventsDB.class.getSimpleName();
    private StopReadingEventsDbHelper mDbHelper;


    public StopReadingEventsDB(Context context){

        this.mDbHelper = new StopReadingEventsDbHelper(context);
    }


    //Methode für den Create Table Befehl
    static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + StopReadingEventsDbContract.StopReadingEventsEntry.TABLE_NAME + " ( " +
                    StopReadingEventsDbContract.StopReadingEventsEntry._ID + " INTEGER PRIMARY KEY," +
                    StopReadingEventsDbContract.StopReadingEventsEntry.COLUMN_NAME_EVENT + " TEXT," +
                    StopReadingEventsDbContract.StopReadingEventsEntry.COLUMN_NAME_TIMESTAMP + " INTEGER)";


    //Methode für den Delete Table Befehl
    static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + StopReadingEventsDbContract.StopReadingEventsEntry.TABLE_NAME;



    public long saveData(Event event){
        //mDbHelper = new BookDbHelper(context);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues dbValues = new ContentValues();
        dbValues.put(StopReadingEventsDbContract.StopReadingEventsEntry.COLUMN_NAME_EVENT, event.getEventName());
        dbValues.put(StopReadingEventsDbContract.StopReadingEventsEntry.COLUMN_NAME_TIMESTAMP, event.getTimestamp());


        long id = db.insert(StopReadingEventsDbContract.StopReadingEventsEntry.TABLE_NAME, null, dbValues);

        return id;
    }


    public String loadData(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = null;
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder =
                StopReadingEventsDbContract.StopReadingEventsEntry._ID + " ASC";

        Cursor cursor = db.query(
                StopReadingEventsDbContract.StopReadingEventsEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        String result = "";
        while(cursor.moveToNext()) {
            result = result + cursor.getInt(
                    cursor.getColumnIndexOrThrow(StopReadingEventsDbContract.StopReadingEventsEntry._ID)) + " "

                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(StopReadingEventsDbContract.StopReadingEventsEntry.COLUMN_NAME_EVENT)) + " "
                    + cursor.getLong(
                    cursor.getColumnIndexOrThrow(StopReadingEventsDbContract.StopReadingEventsEntry.COLUMN_NAME_TIMESTAMP)) + " "
                    +"\n";

        }

        cursor.close();

        return result;
    }

    public void exportDB(Activity activity) {

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            Log.d(TAG, "Permission is not granted");

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Controller.REQUEST_PERMISSION);


        } else {


            File exportDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/BioReader");

            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }

            final String date = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.getDefault()).format(new Date());
            File file = new File(exportDir, "stopReadingEvents_"+date+".csv");

            try {

                file.createNewFile();

                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                SQLiteDatabase db = mDbHelper.getReadableDatabase();
                Cursor curCSV = db.rawQuery("SELECT * FROM StopReadingEventsDB", null);
                csvWrite.writeNext(curCSV.getColumnNames());



                while (curCSV.moveToNext()) {
                    //Which column you want to export
                    String arrStr[] = {curCSV.getString(0), curCSV.getString(1),curCSV.getString(2)};
                    csvWrite.writeNext(arrStr);
                }
                csvWrite.close();
                curCSV.close();
            } catch (Exception sqlEx) {

                Log.d(TAG, sqlEx.getMessage(), sqlEx);
            }
        }
    }

    // Definition der Contract Klasse, in der gewisse Parameter wie Tabellenname festgelegt werden
// hier wird die Struktur der Datenbank aufgebaut
    class StopReadingEventsDbContract {
        // To prevent someone from accidentally instantiating the contract class,
        // make the constructor private.
        private StopReadingEventsDbContract() {}

        /* Inner class that defines the table contents */
        public class StopReadingEventsEntry implements BaseColumns {
            public static final String TABLE_NAME = "StopReadingEventsDB";
            public static final String COLUMN_NAME_EVENT= "Event";
            public static final String COLUMN_NAME_TIMESTAMP = "Timestamp";

        }
    }

    //Unterklasse die SQLiteOpenHelper extended. Dient der Organisation. Man erstellt
//ein Objekt der Klasse FeedReaderDBHelper und kann auf dieses Objekt die Methoden
// getWritableDatabase() or getReadableDatabase() aufrufen
    class StopReadingEventsDbHelper extends SQLiteOpenHelper {
        // If you change the database schema, you must increment the database version.
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "StopReadingEvents.db";

        public StopReadingEventsDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
        }
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }
}