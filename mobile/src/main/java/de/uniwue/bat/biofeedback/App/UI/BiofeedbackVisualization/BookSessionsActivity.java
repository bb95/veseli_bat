package de.uniwue.bat.biofeedback.App.UI.BiofeedbackVisualization;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.ReadingSession;
import de.uniwue.bat.biofeedback.R;

/**
 * Created by BB on 28.04.2018.
 */

public class BookSessionsActivity extends AppCompatActivity {

    public static final String TAG = BookSessionsActivity.class.getSimpleName();

    private String bookUUID;
    private Context context;
    private ListView sessionsListView;
    private ArrayAdapter adapter;
    private ArrayList<String> sessionsListItems;
    private Controller controller;
    private ArrayList<ReadingSession> readingSessions;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sessions);

        sessionsListView = (ListView) findViewById(R.id.sessionsLibraryList);

        bookUUID = getIntent().getExtras().getString(Controller.BOOK_ID);
        context = BookSessionsActivity.this;

        controller = Controller.getInstance(this);
        controller.addObserver(SessionsActivityHandler);

        sessionsListItems = new ArrayList<String>();
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, sessionsListItems);
        sessionsListView.setAdapter(adapter);


        Controller.getInstance(context).postMsg(Controller.GET_SESSIONS, Controller.GET_SESSIONS_TAG, bookUUID);

        sessionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                intent = new Intent(BookSessionsActivity.this, ChartListActivity.class);

                /***
                 * Mit dem Intent wird Position in der Listview bzw. die ID des Textes in der
                 * MetaDatenbank übergeben
                 */

                intent.putExtra(Controller.READING_SESSION, readingSessions.get(i));


                Log.d(TAG, "ReadingSession "+ readingSessions.get(i).getBookUUID()+ "Time: " + readingSessions.get(i).getStartTime() + " " +
                        readingSessions.get(i).getEndTime());

                startActivity(intent);
            }
        });
    }



    Handler SessionsActivityHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){



                case Controller.SEND_SESSIONS:

                    sessionsListItems.clear();

                    Bundle sessionsBundle = msg.getData();
                    readingSessions = (ArrayList<ReadingSession>) sessionsBundle.getSerializable(Controller.SEND_SESSIONS_TAG);

                    ArrayList<String> sessionsInventoryStr = new ArrayList<String>();


                    SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss ");
                    SimpleDateFormat formatDay = new SimpleDateFormat("EEE, dd.MM.yy" )
                    ;

                    for(ReadingSession readSession : readingSessions){
                        sessionsInventoryStr.add(
                                "Session: " +
                                formatDay.format(new Date(readSession.getStartTime()))+ ": "+
                                formatTime.format(new Date(readSession.getStartTime()))  + " " +
                                formatTime.format(new Date(readSession.getEndTime()))
                        );
                    }

                    Log.d(TAG, "SessionsList " + sessionsInventoryStr);

                    sessionsListItems.addAll(sessionsInventoryStr);
                    adapter.notifyDataSetChanged();

                    break;


            }


        }
    };

}
