package de.uniwue.bat.biofeedback.App.UI;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Databases.BookSessionsDB;
import de.uniwue.bat.biofeedback.App.Databases.UserDB;
import de.uniwue.bat.biofeedback.App.Databases.VideoSessionsDB;
import de.uniwue.bat.biofeedback.App.Databases.BookDB;
import de.uniwue.bat.biofeedback.App.Databases.PageSessionsDB;
import de.uniwue.bat.biofeedback.App.Databases.SensorDB;
import de.uniwue.bat.biofeedback.App.Databases.StopReadingEventsDB;
import de.uniwue.bat.biofeedback.App.Databases.PagesDB;
import de.uniwue.bat.biofeedback.R;

/**
 * Created by BB on 13.04.2018.
 */

public class ExportActivity  extends AppCompatActivity{

    private Button exportBtn;
    private BookDB metaDB;
    private PageSessionsDB readingProgressDB;
    private SensorDB sensorDB;
    private StopReadingEventsDB stopReadingEventsDB;
    private PagesDB textDatabase;
    private BookSessionsDB bookSessionDB;
    private VideoSessionsDB measurementSessionDB;
    private UserDB userDB;

    private Context context;
    private final String TAG = ExportActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export);

        //kommentar
        context = ExportActivity.this;

        exportBtn = (Button) findViewById(R.id.exportActBtn);

        exportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                exportAllDatabases();
                Toast.makeText(getApplicationContext(), "Data exported as csv!", Toast.LENGTH_LONG).show();


            }
        });
    }


    //Merke: Alle haben dieselben Controller.Request_Permission Code!! Könnte evtl Probleme geben
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Controller.REQUEST_PERMISSION: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    exportAllDatabases();

                } else {

                    Log.d(TAG, "Permission denied again");

                    //Toast.makeText(ExportActivity.this, "Without a permission saving file is not possible", Toast.LENGTH_LONG).show();

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void exportAllDatabases(){

        metaDB = new BookDB(context);
        metaDB.exportDB(ExportActivity.this);

        readingProgressDB = new PageSessionsDB(context);
        readingProgressDB.exportDB(ExportActivity.this);

        sensorDB = new SensorDB(context);
        sensorDB.exportDB(ExportActivity.this);

        stopReadingEventsDB = new StopReadingEventsDB(context);
        stopReadingEventsDB.exportDB(ExportActivity.this);

        textDatabase = new PagesDB(context);
        textDatabase.exportDB(ExportActivity.this);

        bookSessionDB = new BookSessionsDB(context);
        bookSessionDB.exportDB(ExportActivity.this);

        measurementSessionDB = new VideoSessionsDB(context);
        measurementSessionDB.exportDB(ExportActivity.this);

        userDB = new UserDB(context);
        userDB.exportDB(ExportActivity.this);

    }
}
