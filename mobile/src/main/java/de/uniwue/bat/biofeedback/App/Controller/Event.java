package de.uniwue.bat.biofeedback.App.Controller;

import java.io.Serializable;

/**
 * Created by BB on 04.04.2018.
 */

public class Event implements Serializable {


    public String eventName;
    public long timestamp;

    /**
     * this creates a event object that contains eventname and the timestamp when this event happened
     * @param eventName represents the endingEvents of a booksession (Events like: tab/home or backpressbutton pressend, screen off)
     * @param timeStamp represents
     */
    public Event(String eventName, long timeStamp){

        this.eventName = eventName;
        this.timestamp= timeStamp;

    }


    public String getEventName() {
        return eventName;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
