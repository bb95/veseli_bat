package de.uniwue.bat.biofeedback.App.Databases;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.provider.BaseColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.Page;


public class PageSessionsDB {

    private static final String TAG = PageSessionsDB.class.getSimpleName();
    private PageSessionsDbHelper mDbHelper;


    public PageSessionsDB(Context context){

        this.mDbHelper = new PageSessionsDbHelper(context);
    }


    //Methode für den Create Table Befehl
    static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + PageSessionsDbContract.PageSessionsEntry.TABLE_NAME + " ( " +
                    PageSessionsDbContract.PageSessionsEntry._ID + " INTEGER PRIMARY KEY," +
                    PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_BOOK_ID + " TEXT," +
                    PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_PAGE_NUM + " INTEGER,"+
                    PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_TIMESTAMP_START + " INTEGER,"+
                    PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_TIMESTAMP_END + " INTEGER)";

    //Methode für den Delete Table Befehl
    static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + PageSessionsDbContract.PageSessionsEntry.TABLE_NAME;



    long saveData(String bookUUID, int pageNum, long timestampStart){

        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues dbValues = new ContentValues();
        dbValues.put(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_BOOK_ID,bookUUID);
        dbValues.put(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_PAGE_NUM, pageNum);
        dbValues.put(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_TIMESTAMP_START, timestampStart);
        //End Timestamp will be updated with method saveEndingTimestamp()
        dbValues.put(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_TIMESTAMP_END, timestampStart);

        long id = db.insert(PageSessionsDbContract.PageSessionsEntry.TABLE_NAME, null, dbValues);
        db.close();

        return id;

    }

    void saveEndingTimestamp(String bookUUID, int pageNum, long timestampEnd, long id){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        String idAsString = id +"";
        String pageNumStr = pageNum +"";
        //String bookIDStr = bookID+"";
        String[] whereArgs = new String[] {idAsString,pageNumStr,bookUUID };



        ContentValues dbValues = new ContentValues();
        dbValues.put(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_TIMESTAMP_END, timestampEnd);

        db.update(PageSessionsDbContract.PageSessionsEntry.TABLE_NAME,dbValues, PageSessionsDbContract.PageSessionsEntry._ID + " = ? AND " +
                PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_PAGE_NUM + " = ? AND " + PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_BOOK_ID +
                " = ?", whereArgs);

        db.close();
    }

    public void deleteData(String uuid){

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        //String idAsString = id +"";

        String[] whereArgs = new String[] {uuid};


        db.delete(PageSessionsDbContract.PageSessionsEntry.TABLE_NAME, PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_BOOK_ID + " = ?", whereArgs);
        db.close();

    }

    public int getLastPage(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String[] projection = null;
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder =
                    PageSessionsDbContract.PageSessionsEntry._ID + " ASC";

        Cursor cursor = db.query(
                    PageSessionsDbContract.PageSessionsEntry.TABLE_NAME,                     // The table to query
                    projection,                               // The columns to return
                    selection,                                // The columns for the WHERE clause
                    selectionArgs,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder                                 // The sort order
        );



        if(cursor.moveToLast()){

            int result = cursor.getInt(
                        cursor.getColumnIndexOrThrow(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_PAGE_NUM));

            return result;
        }


        else{
            cursor.close();
            return 0;

        }

    }

    public String loadData(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = null;
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder =
                PageSessionsDbContract.PageSessionsEntry._ID + " ASC";

        Cursor cursor = db.query(
                PageSessionsDbContract.PageSessionsEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        String result = "";
        while(cursor.moveToNext()) {
            result = result + cursor.getInt(
                    cursor.getColumnIndexOrThrow(PageSessionsDbContract.PageSessionsEntry._ID)) + " "

                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_BOOK_ID)) + " "
                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_PAGE_NUM)) + " "
                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_TIMESTAMP_START)) + " "
                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_TIMESTAMP_END)) + " "

                    +"\n";

        }

        cursor.close();

        return result;
    }

    public ArrayList<Page> loadDataAsArrayList(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = null;
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder =
                PageSessionsDbContract.PageSessionsEntry._ID + " ASC";

        Cursor cursor = db.query(
                PageSessionsDbContract.PageSessionsEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        ArrayList<Page> result = new ArrayList<Page>();
        while(cursor.moveToNext()) {
            result.add(new Page(cursor.getString(cursor.getColumnIndexOrThrow(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_BOOK_ID)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_PAGE_NUM)),
                    cursor.getLong(cursor.getColumnIndexOrThrow(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_TIMESTAMP_START)),
                    cursor.getLong(cursor.getColumnIndexOrThrow(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_TIMESTAMP_END))
                            )

            );

        }

        cursor.close();

        return result;
    }

    public int getPagesByTimestamp(long timestamp) {

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_PAGE_NUM + " FROM " +
                PageSessionsDbContract.PageSessionsEntry.TABLE_NAME + " WHERE " + PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_TIMESTAMP_START
                + " <= " + timestamp + " AND " + PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_TIMESTAMP_END + " >= " + timestamp, null);

        int pageNum=0;
        while (cursor.moveToNext()) {
            //Which column you want to export

           pageNum = cursor.getInt(cursor.getColumnIndexOrThrow(PageSessionsDbContract.PageSessionsEntry.COLUMN_NAME_PAGE_NUM));
        }
        cursor.close();
        return pageNum;

    }


    public void exportDB(Activity activity) {

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            Log.d(TAG, "Permission is not granted");

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Controller.REQUEST_PERMISSION);


        } else {


            File exportDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/BioReader");

            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }

            final String date = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.getDefault()).format(new Date());
            File file = new File(exportDir, "pageSessionsDB_"+date+".csv");

            try {

                file.createNewFile();

                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                SQLiteDatabase db = mDbHelper.getReadableDatabase();
                Cursor curCSV = db.rawQuery("SELECT * FROM pageSessionsDB", null);
                csvWrite.writeNext(curCSV.getColumnNames());



                while (curCSV.moveToNext()) {
                    //Which column you want to export
                    String arrStr[] = {curCSV.getString(0), curCSV.getString(1), curCSV.getString(2),curCSV.getString(3),curCSV.getString(4)};
                    csvWrite.writeNext(arrStr);
                }
                csvWrite.close();
                curCSV.close();
            } catch (Exception sqlEx) {

                Log.d(TAG, sqlEx.getMessage(), sqlEx);
            }
        }
    }


    // Definition der Contract Klasse, in der gewisse Parameter wie Tabellenname festgelegt werden
// hier wird die Struktur der Datenbank aufgebaut
    class PageSessionsDbContract {
        // To prevent someone from accidentally instantiating the contract class,
        // make the constructor private.
        private PageSessionsDbContract() {}

        /* Inner class that defines the table contents */
        public class PageSessionsEntry implements BaseColumns {
            public static final String TABLE_NAME = "pageSessionsDB";
            public static final String COLUMN_NAME_BOOK_ID= "bookUUID";
            public static final String COLUMN_NAME_PAGE_NUM = "pagenumber";
            public static final String COLUMN_NAME_TIMESTAMP_START = "startTimestamp";
            public static final String COLUMN_NAME_TIMESTAMP_END = "endTimestamp";




        }
    }

    //Unterklasse die SQLiteOpenHelper extended. Dient der Organisation. Man erstellt
//ein Objekt der Klasse FeedReaderDBHelper und kann auf dieses Objekt die Methoden
// getWritableDatabase() or getReadableDatabase() aufrufen
    class PageSessionsDbHelper extends SQLiteOpenHelper {
        // If you change the database schema, you must increment the database version.
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "PageSessions.db";

        public PageSessionsDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
        }
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }
}