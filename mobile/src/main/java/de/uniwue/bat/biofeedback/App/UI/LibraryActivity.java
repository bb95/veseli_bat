package de.uniwue.bat.biofeedback.App.UI;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.MenuItem;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import de.uniwue.bat.biofeedback.App.Controller.Book;
import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.User;
import de.uniwue.bat.biofeedback.R;



public class LibraryActivity extends AppCompatActivity {

    private static final String TAG = LibraryActivity.class.getSimpleName();

    private ListView libraryListView;
    private FloatingActionButton addBookButton;
    private Intent fileChooser;
    final int requcode = 3;
    private Uri texturi;
    private InputStream is;
    private ArrayAdapter adapter;
    private ArrayList<String> libraryListItems;
    private StringBuilder text;
    private Controller controller;
    Intent intent;
    private ArrayList<Book> libraryInventory;
    boolean camOK;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);


        controller = Controller.getInstance(this);
        controller.addObserver(LibraryActivityHandler);



        /***
         * Erstellung und Befüllen der Listview
         */

        libraryListView = (ListView) findViewById(R.id.libraryList);

        libraryListItems = new ArrayList<String>();
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, libraryListItems);
        libraryListView.setAdapter(adapter);

        registerForContextMenu(libraryListView);


        //updaten der Bibliothek
        controller.getControllerHandler().sendEmptyMessage(Controller.UPDATE_LIBRARY);


        libraryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                  Log.d(TAG, "Book PositionID"+ libraryInventory.get(i).getBookID()+"");

                showCameraHintDialog(LibraryActivity.this, libraryInventory.get(i).getBookUUID());

            }
        });

        /***
         * Erstellung der Toolbar
         * Wird aber noch nicht verwendet
         */

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /***
         * Erstellung des File Choosers
         * Floating Action Button wird mit der Funktion ausgestattet,
         * Dateien aus dem Speicher auszuwählen
         */

        addBookButton = (FloatingActionButton) findViewById(R.id.addBook);

        addBookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /***
                 * File Chooser wird mithilfe eines Intents geöffnet
                 */

                fileChooser = new Intent(Intent.ACTION_GET_CONTENT);
                fileChooser.setType("*/*");
                startActivityForResult(fileChooser,requcode );
            }
        });
    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(0, v.getId(), 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        int indexSelected = info.position;
        String uuid = libraryInventory.get(indexSelected).getBookUUID();

        if (item.getTitle() == "Delete") {

            Log.d(TAG, "IndexSelected "+indexSelected);
            Log.d(TAG, "SelectedBook UUID "+uuid);

            controller.postMsg(Controller.DELETE_BOOK, Controller.DELETE_BOOK_TAG, uuid);

            libraryListItems.remove(indexSelected);
            adapter.notifyDataSetChanged();
        }
        return true;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){

            if(requestCode == requcode){

                texturi = data.getData();
                try {
                    is = getContentResolver().openInputStream(texturi);



                    text = new StringBuilder();
                    try {
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));
                        String line;
                        while ((line = br.readLine()) != null) {
                            text.append(line);
                            text.append('\n');
                        }
                    }

                    catch (IOException e) {
                        Toast.makeText(getApplicationContext(),"Error reading file!",Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }

                    /***
                     * Öffnen das Input-Fensters der Meta Daten
                     * Der Text, der durch die texturi erhalten wird, wird als String mitgeschickt
                     */
                    openDialog(text.toString());


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            }


        }


    }

    public void showCameraHintDialog(Context context, final String uuid){
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.dialog_camera_hint, null);

        builder.setView(view)
                .setNegativeButton("Keine Aufnahmen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //camera ausmachen

                        //The function return true
                        intent = new Intent(LibraryActivity.this, EreaderActivity.class);

                        Log.d(TAG, "Book UUID "+ uuid);

                        Bundle extras = new Bundle();
                        extras.putString(Controller.BOOK_ID,uuid);
                        extras.putBoolean(Controller.VIDEO_CAPTURE,false);

                        intent.putExtras(extras);
                        startActivity(intent);

                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //The function return true
                        intent = new Intent(LibraryActivity.this, EreaderActivity.class);

                        /***
                         * Mit dem Intent wird Position in der Listview bzw. die ID des Textes in der
                         * MetaDatenbank übergeben
                         */

                        Log.d(TAG, "Book UUID "+ uuid);

                        Bundle extras = new Bundle();
                        extras.putString(Controller.BOOK_ID,uuid);
                        extras.putBoolean(Controller.VIDEO_CAPTURE,true);

                        intent.putExtras(extras);
                        startActivity(intent);

                    }
                });


        AlertDialog alert = builder.create();
        alert.show();


    }


    /***
     * Es wird der Input-Dialog geöffnet, in dem man Autor und Titel angeben kann
     * @param text ist der Text als String, der ausgewählt wird und geöffnet werden soll
     */
    public void openDialog(String text){

        AddBookDialog metadataDialog = new AddBookDialog();

        Bundle args = new Bundle();
        args.putString(Controller.SELECTED_TEXT, text);
        metadataDialog.setArguments(args);
        metadataDialog.show(getSupportFragmentManager(), Controller.META_DATA_DIALOG);
    }

    Handler LibraryActivityHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){



                case Controller.LIBRARY_CONTENT:

                    libraryListItems.clear();

                    Bundle libraryBundle = msg.getData();
                    libraryInventory = (ArrayList<Book>) libraryBundle.getSerializable(Controller.LIBRARY_TAG);

                    ArrayList<String> libraryInventoryStr = new ArrayList<String>();

                    for(Book b : libraryInventory){
                        libraryInventoryStr.add(b.getAuthor()+" "+ b.getTitle());
                    }

                    Log.d("MainActivity", "Meta DB ROWS " + libraryInventoryStr);
                    libraryListItems.addAll(libraryInventoryStr);
                    adapter.notifyDataSetChanged();
                    break;



            }


        }
    };
}
