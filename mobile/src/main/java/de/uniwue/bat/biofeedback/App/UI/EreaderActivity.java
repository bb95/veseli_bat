/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.uniwue.bat.biofeedback.App.UI;


import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import de.uniwue.bat.biofeedback.App.Controller.Book;
import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.Event;
import de.uniwue.bat.biofeedback.App.Controller.Page;
import de.uniwue.bat.biofeedback.App.Functions.ScreenReceiver;
import de.uniwue.bat.biofeedback.R;
import de.uniwue.bat.biofeedback.shared.ClientPaths;


public class EreaderActivity extends FragmentActivity{

    static final String TAG = EreaderActivity.class.getSimpleName();
    private static final int CLIENT_CONNECTION_TIMEOUT = 15000;

    private ExecutorService executorService;
    private GoogleApiClient googleApiClient;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    Context context;
    private String bookId;
    private String author;
    private String title;
    private ArrayList<String> pages;
    private int NUM_PAGES;
    private Thread readingProgressThread;
    private Runnable readingProgressRunnable;
    private BroadcastReceiver mReceiver;
    private Runnable sensorReceiverRunnable;
    private Thread sensorReceiverThread;

    //a variable to control the camera
    private Camera mCamera;
    private MediaRecorder mediaRecorder;
    CameraPreview mPreview;
    private boolean isRecording;
    public static final int MEDIA_TYPE_VIDEO = 2;
    ImageButton startVideo;
    TextView videoInfo;

    private Boolean videoCapture;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ereader);

        //turn off camera sound
        AudioManager mgr = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mgr.setStreamVolume(AudioManager.STREAM_SYSTEM,0,0);
            mgr.adjustStreamVolume(AudioManager.STREAM_SYSTEM,AudioManager.ADJUST_MUTE, 0);
        } else {
            mgr.setStreamMute(AudioManager.STREAM_SYSTEM, true);
        }

        mCamera = getFrontFacingCamera();
        mPreview = new CameraPreview(this, mCamera);

        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        videoCapture = extras.getBoolean(Controller.VIDEO_CAPTURE);
        bookId = extras.getString(Controller.BOOK_ID);
        Log.d(TAG, "video capture is " + videoCapture);


        //bookId = getIntent().getExtras().getString(Controller.BOOK_ID);
        Log.d(TAG,"Book ID "+bookId);

        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);

        filter.addAction(Intent.ACTION_SCREEN_OFF);
        mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);


        readingProgressThread = null;


        readingProgressRunnable = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Thread - Runnable run()-method ");
                int pageNum = mPager.getCurrentItem()+1;

                try {
                    Thread.sleep(800);

                    Log.d(TAG, "Thread startet nach 800 ms , Seite: "+ pageNum);
                    long startTime = System.currentTimeMillis();

                    Page p = new Page(bookId, pageNum, startTime);
                    Log.d(TAG, "OnPageSelected() - CurrentItem: " + pageNum + " Start Time Page: " + startTime);

                    Controller.getInstance(context).postMsg(Controller.START_PAGE, Controller.START_PAGE_TAG, p);


                    int j = 0;

                    while (!readingProgressThread.isInterrupted()) {

                        Log.d(TAG, "Thread - sekündlich , Seite: "+pageNum);

                        long endTime = System.currentTimeMillis();
                        Page p1 = new Page(bookId, pageNum, endTime);
                        Controller.getInstance(context).postMsg(Controller.END_PAGE, Controller.END_PAGE_TAG, p1);
                        Thread.sleep(1000);

                        Log.d(TAG, "Counter läuft: " + j);





                        j++;
                    }
                } catch (InterruptedException e) {
                    Log.d(TAG, "Thread - Runnable catch()");
                }

            }
        };

        sensorReceiverRunnable = new Runnable() {
            @Override
            public void run() {
                Log.d("ScreenSlideAct", "startMeasurement");
                controlMeasurementInBackground(ClientPaths.START_MEASUREMENT);

            }
        };



        Log.d(TAG, "Lifecycle - onCreate");

        Controller.getInstance(context).addObserver(screenSlideActivityHandler);





        Controller.getInstance(context).postMsg(Controller.GET_BOOK_DATA, Controller.GET_BOOK_DATA_TAG, bookId);

        mPager = (ViewPager) findViewById(R.id.pager);


        this.googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();

        this.executorService = Executors.newCachedThreadPool();

        sensorReceiverThread = new Thread(sensorReceiverRunnable);
        sensorReceiverThread.start();


        //starten der Videoaufnahme
        startVideo = (ImageButton) findViewById(R.id.startVideoBtn);
        videoInfo = (TextView) findViewById(R.id.video_info);

        Log.d(TAG,"onCreate() - isRecording"+ isRecording);
        startVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (isRecording) {
                    // stop recording and release camera
                    mediaRecorder.stop();  // stop the recording
                    releaseMediaRecorder(); // release the MediaRecorder object
                    mCamera.lock();         // take camera access back from MediaRecorder

                    // inform the user that recording has stopped
                    //startVideo.setText("Capture");
                    startVideo.setImageResource(R.drawable.play_circle_black);
                    videoInfo.setText("\n video capturing is OFF");

                    //setCaptureButtonText("Capture");

                    isRecording = false;
                    Log.d(TAG, "Capture"+isRecording);
                } else {
                    // initialize video camera
                    if (prepareVideoRecorder()) {

                        //prepareVideoRecorder geht nicht, invalid preview surface
                        //daher springt er direkt in die nächste else und released die recorder

                        // Camera is available and unlocked, MediaRecorder is prepared,
                        // now you can start recording
                        mediaRecorder.start();

                        // inform the user that recording has started
                        //setCaptureButtonText("Stop");
                        //startVideo.setText("Stop");
                        startVideo.setImageResource(R.drawable.pause_circle_black);
                        videoInfo.setText("\n video capturing is ON");

                        isRecording = true;
                        Log.d(TAG, "Stop"+isRecording);
                    } else {
                        // prepare didn't work, release the camera
                        releaseMediaRecorder();
                        Log.d(TAG, "Release "+isRecording);
                        // inform user
                    }
                }



            }
        });




    }




    /**
     * A simple pager adapter that represents 5 {@link PageFragment} objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(final int pageNum) {
            Log.d(TAG, "getItem- FragmentAdapter");


            Bundle args = new Bundle();
            Fragment mFragment = PageFragment.create();
            Book b = new Book(pageNum, pages.get(pageNum),author, title, NUM_PAGES);
            args.putSerializable(Controller.BOOK_DATA, b);
            mFragment.setArguments(args);

            return mFragment;


        }

        @Override
        public int getCount() {
            Log.d(TAG, "getCount - FragmentAdapter"+ NUM_PAGES);
            return NUM_PAGES;
        }

        @Override
        public void finishUpdate(ViewGroup container) {
            try{
                super.finishUpdate(container);
            } catch (NullPointerException nullPointerException){
                System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
            }
        }


    }


    Handler screenSlideActivityHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case Controller.SEND_BOOK_DATA:

                    Bundle bookBundle = msg.getData();
                    Book book = (Book) bookBundle.getSerializable(Controller.SEND_BOOK_DATA_TAG);
                    author = book.getAuthor();
                    title = book.getTitle();
                    pages = book.getPages();
                    NUM_PAGES = pages.size();

                    mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());

                    mPager.setAdapter(mPagerAdapter);



                    mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener(){
                        Boolean first = true;


                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                            Log.d(TAG,"onPageScrolled() "+position);

                            int p = position;
                            Log.d(TAG, "Scrolled Page "+p);

                            if (first && positionOffset == 0 && positionOffsetPixels == 0){
                                onPageSelected(0);
                                first = false;
                            }
                        }

                        @Override
                        public void onPageSelected(final int position) {

                            Log.d(TAG, "onPageSelected() CurrenItem"+ mPager.getCurrentItem());


                            if(readingProgressThread != null){
                                Log.d(TAG, "Thread ist nicht null");
                                Log.d(TAG, "Thread wird beendet");
                                readingProgressThread.interrupt();
                                try {
                                    Thread.sleep(10);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                            readingProgressThread = new Thread(readingProgressRunnable);
                            readingProgressThread.start();

                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {
                            Log.d(TAG, "onPageScrollStateChanged() "+state);

                        }
                    });


                    Log.d(TAG, "SEND BOOK DATA: "+author+title);


                    break;


            }
        }
    };


    private boolean validateConnection() {
        if (googleApiClient.isConnected()) {
            return true;
        }

        ConnectionResult result = googleApiClient.blockingConnect(CLIENT_CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);

        return result.isSuccess();
    }

    public void stopMeasurement() {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                Log.d("ScreenSlideAct", "stopMeasurement");
                controlMeasurementInBackground(ClientPaths.STOP_MEASUREMENT);
            }
        });
    }

    private void controlMeasurementInBackground(final String path) {
        if (validateConnection()) {
            List<Node> nodes = Wearable.NodeApi.getConnectedNodes(googleApiClient).await().getNodes();

            Log.d(TAG, "Sending to nodes: " + nodes.size());

            for (Node node : nodes) {
                Log.i(TAG, "add node " + node.getDisplayName());
                //Verbindung müsste eigentlich in Zeile 136 gestartet werden, setResultCallback startet nur einen Log Eintrag
                //der Log Eintrag, der bei setResultCallback gestartet wird, besagt, dass die Message erfolgreich gesendet wurde
                Wearable.MessageApi.sendMessage(googleApiClient, node.getId(), path, null)
                        .setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                            @Override
                            public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                Log.d(TAG, "controlMeasurementInBackground(" + path + "): " + sendMessageResult.getStatus().isSuccess());
                            }
                        });
            }
        } else {
            Log.w(TAG, "No connection possible");
        }
    }




    @Override
    protected void onResume() {
        super.onResume();

        //Wird aufgerufen wenn App per Home Button in den Hintergrund kommt und dann wieder
        // hervorgeholt wird


        Log.d(TAG, "Lifecycle - onResume()");
        long bookStartTime = System.currentTimeMillis();
        Log.d(TAG, "onResume() Book ID "+ bookId);
        Event startBook = new Event(bookId,bookStartTime);

        Controller.getInstance(EreaderActivity.this).postMsg(Controller.SEND_START_BOOK_EVENT, Controller.SEND_START_BOOK_EVENT_TAG, startBook);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        long backpressTimestamp = System.currentTimeMillis();

        Event backpressEvent = new Event("Backpress", backpressTimestamp);
        Controller.getInstance(context).postMsg(Controller.SEND_BACKPRESS_EVENT, Controller.SEND_BACKPRESS_EVENT_TAG, backpressEvent);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        //wird aufgerufen, wenn Screen aus ist oder Homebutton gedrückt wird und das Frag aus
        //den Tabs wieder hervorgeholt wird
        Log.d(TAG, "Lifecycle - onRestart(), Seite: "+mPager.getCurrentItem());

        //Restart Measurement of Reading Progress
        readingProgressThread = new Thread(readingProgressRunnable);
        readingProgressThread.start();

        //Restart Measurement of Sensor Data
        sensorReceiverThread = new Thread(sensorReceiverRunnable);
        sensorReceiverThread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d(TAG, "Lifecycle - onPause()");
        //Log.d(TAG, "Aktuelle Seite: "+mPager.getCurrentItem());

        stopMeasurement();
        Log.d(TAG, "Sensor Measurement stops");


        readingProgressThread.interrupt();

        if (isApplicationSentToBackground(this)){
            // will be executed when home or tab button got pressed
            long eventTimestamp = System.currentTimeMillis();
            Event homeTabEvent = new Event("HomeTabButton", eventTimestamp);
            Controller.getInstance(context).postMsg(Controller.SEND_HOME_TAB_EVENT, Controller.SEND_HOME_TAB_EVENT_TAG, homeTabEvent );

        }

        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();



    }

    public boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "Lifecycle - onStop()");
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Lifecycle - onDestroy()");
        unregisterReceiver(mReceiver);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Lifecycle - onStart()");


    }



    public void jumpToPreviousPage(View view) {
        mPager.setCurrentItem(mPager.getCurrentItem() - 1, true);

        changePage();


    }

    public void jumpToNextPage(View view) {
        mPager.setCurrentItem(mPager.getCurrentItem() + 1, true);

        changePage();

    }

    public void finishActivity(View view) {
        finish();

    }

    public void changePage(){

        if(readingProgressThread != null){
            Log.d(TAG, "Thread ist nicht null");
            Log.d(TAG, "Thread wird beendet");
            readingProgressThread.interrupt();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        readingProgressThread = new Thread(readingProgressRunnable);
        readingProgressThread.start();

    }


    private Camera getFrontFacingCamera() {
        int cameraCount = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    cam = Camera.open(camIdx);
                    Log.d(TAG, "FrontCam opened");
                } catch (RuntimeException e) {
                    Log.e("MainActivity", "Camera failed to open: " + e.getLocalizedMessage());
                }
            }
        }

        return cam;
    }


    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){


        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/saved_videos");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        long timestamp = System.currentTimeMillis();
        File mediaFile;
        if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timestamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }


    public boolean isStoragePermissionGranted() {
        String TAG = "Storage Permission";
        if (Build.VERSION.SDK_INT >= 23) {
            if (this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }


    /** A basic Camera preview class */
    public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
        private SurfaceHolder mHolder;
        private Camera mCamera;

        public CameraPreview(Context context, Camera camera) {
            super(context);
            mCamera = camera;

            // Install a SurfaceHolder.Callback so we get notified when the
            // underlying surface is created and destroyed.
            mHolder = getHolder();
            mHolder.addCallback(this);
            // deprecated setting, but required on Android versions prior to 3.0
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        public void surfaceCreated(SurfaceHolder holder) {
            // The Surface has been created, now tell the camera where to draw the preview.
            try {

                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
                Log.d(TAG, "surfaceCreated()- successfully");

            } catch (IOException e) {
                Log.d(TAG, "surfaceCreated() - Error setting camera preview: " + e.getMessage());
            }

            if (videoCapture){

                if (prepareVideoRecorder()){
                    mediaRecorder.start();

                    startVideo.setImageResource(R.drawable.pause_circle_black);
                    videoInfo.setText("\n video capturing is ON");
                    isRecording = true;
                }

            }



        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            // empty. Take care of releasing the Camera preview in your activity.
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
            // If your preview can change or rotate, take care of those events here.
            // Make sure to stop the preview before resizing or reformatting it.

            if (mHolder.getSurface() == null){
                Log.d(TAG, "surfaceChanged() - mHolder.getSurface == null");
                // preview surface does not exist
                return;
            }

//            // stop preview before making changes
//            try {
//                mCamera.stopPreview();
//                Log.d(TAG, "surfaceChanged() - stopPreview()");
//            } catch (Exception e){
//                // ignore: tried to stop a non-existent preview
//            }
//
//            // set preview size and make any resize, rotate or
//            // reformatting changes here
//
//            // start preview with new settings
//            try {
//
//                mCamera.setPreviewDisplay(mHolder);
//                mCamera.startPreview();
//                Log.d(TAG, "surfaceChanged() - startPreview()");
//
//            } catch (Exception e){
//                Log.d(TAG, "surfaceChanged() - Error starting camera preview: " + e.getMessage());
//            }
        }
    }

    private boolean prepareVideoRecorder(){

        mCamera = getFrontFacingCamera();
        mediaRecorder = new MediaRecorder();

        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        mediaRecorder.setCamera(mCamera);

        // Step 2: Set sources
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));

        // Step 4: Set output file
        mediaRecorder.setOutputFile(getOutputMediaFile(MEDIA_TYPE_VIDEO).toString());

        // Step 5: Set the preview output
        mediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

        // Step 6: Prepare configured MediaRecorder
        try {
            mediaRecorder.prepare();
            Log.d(TAG, "MediaRecorder - mediaRecorder.prepare()");
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private void releaseMediaRecorder(){
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }



}
