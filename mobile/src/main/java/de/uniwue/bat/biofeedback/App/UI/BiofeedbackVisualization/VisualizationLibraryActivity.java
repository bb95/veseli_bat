package de.uniwue.bat.biofeedback.App.UI.BiofeedbackVisualization;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import de.uniwue.bat.biofeedback.App.Controller.Book;
import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.R;

/**
 * Created by BB on 18.04.2018.
 */

public class VisualizationLibraryActivity extends AppCompatActivity {

    private static final String TAG = VisualizationLibraryActivity.class.getSimpleName();

    private ListView libraryListview;
    private ArrayAdapter adapter;
    private ArrayList<String> libraryListItems;
    private Controller controller;
    private ArrayList<Book> libraryInventory;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualization_library);

        libraryListview = (ListView) findViewById(R.id.visualizationLibraryList);

        controller = Controller.getInstance(this);
        controller.addObserver( LibraryVisualizationActivityHandler);

        libraryListItems = new ArrayList<String>();
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, libraryListItems);
        libraryListview.setAdapter(adapter);

        //updaten der Bibliothek
        controller.getControllerHandler().sendEmptyMessage(Controller.UPDATE_LIBRARY);


        libraryListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                intent = new Intent(VisualizationLibraryActivity.this, BookSessionsActivity.class);

                intent.putExtra(Controller.BOOK_ID, libraryInventory.get(i).getBookUUID());

                startActivity(intent);
            }
        });
    }


    Handler LibraryVisualizationActivityHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){



                case Controller.LIBRARY_CONTENT:

                    libraryListItems.clear();

                    Bundle libraryBundle = msg.getData();
                    libraryInventory = (ArrayList<Book>) libraryBundle.getSerializable(Controller.LIBRARY_TAG);

                    ArrayList<String> libraryInventoryStr = new ArrayList<String>();

                    for(Book b : libraryInventory){
                        libraryInventoryStr.add(b.getAuthor()+" "+ b.getTitle());
                    }

                    Log.d("MainActivity", "Meta DB ROWS " + libraryInventoryStr);
                    libraryListItems.addAll(libraryInventoryStr);
                    adapter.notifyDataSetChanged();
                    break;


            }


        }
    };
}
