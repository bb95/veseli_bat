package de.uniwue.bat.biofeedback.App.Controller;


import java.io.Serializable;

/**
 * Created by BB on 28.04.2018.
 */

public class ReadingSession implements Serializable {


    String bookUUID;
    long startTime;
    long endTime;


    public ReadingSession(){

    }

    /**
     * this created a reading session object that contains the uuid of the book that got started, the start timestamp
     * and the end timestamp of the book
     * @param bookUUID represents the uuid of the book
     * @param startTime represents the time, when the book started
     * @param endTime represents the time when the book closed
     */
    public ReadingSession(String bookUUID, long startTime, long endTime){
        this.bookUUID = bookUUID;
        this.startTime = startTime;
        this.endTime = endTime;

    }



    public String getBookUUID() {
        return bookUUID;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }
}
