package de.uniwue.bat.biofeedback.App.Controller;

import java.sql.Array;
import java.util.ArrayList;

/**
 * Created by BB on 06.07.2018.
 */

public class Datapoint {

    //timestamp of every recorded sensor value
    long x;

    //sensor value
    String strValues;
    ArrayList<Float> y;

    //channel of a sensor value/timestamp - pair
    int channel;

    //sensor id of the sensor
    int id;
    int page;

    /**
     * this creates a data point object saving and transfering the sensor data
     * @param x represents the timestamp
     * @param strValues represents the sensorrates
     * @param id represents the sensorid
     */
    public Datapoint(long x, String strValues, int id){

        this.x = x;
        this.strValues = strValues;
        this.id = id;

        String[] sensorValue =strValues.split("//");

        y = new ArrayList<Float>();
        for(String value : sensorValue){
            y.add(Float.parseFloat(value));
        }


    }

    public long getX() {
        return x;
    }

    public ArrayList<Float> getY() {
        return y;
    }

    public int getId() {
        return id;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }
}
