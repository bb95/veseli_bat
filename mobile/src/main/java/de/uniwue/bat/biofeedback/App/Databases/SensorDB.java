package de.uniwue.bat.biofeedback.App.Databases;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.provider.BaseColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.Sensor;
import de.uniwue.bat.biofeedback.App.Controller.Datapoint;

public class SensorDB {
    //instanz von Unterklasse die SQLHelper beinhaltet, wird nur festgelegt. Ist bisher leer,
    SensorDbHelper mDbHelper;
    public final static String TAG = SensorDB.class.getSimpleName();

    public SensorDB(Context context) {

        this.mDbHelper = new SensorDbHelper(context);
    }


    //Methode für den Create Table Befehl
    static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + SensorDbContract.SensorDbEntry.TABLE_NAME + " ( " +
                    SensorDbContract.SensorDbEntry._ID + " INTEGER PRIMARY KEY," +
                    SensorDbContract.SensorDbEntry.COLUMN_NAME_SENSOR_ID + " INTEGER," +
                    SensorDbContract.SensorDbEntry.COLUMN_NAME_SENSOR_RATES + " TEXT," +
                    SensorDbContract.SensorDbEntry.COLUMN_NAME_TIMESTAMP + " INTEGER," +
                    SensorDbContract.SensorDbEntry.COLUMN_NAME_ACCURACY + " INTEGER)";
    ;


    //Methode für den Delete Table Befehl
    static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + SensorDbContract.SensorDbEntry.TABLE_NAME;

    // WAAAAAAAAARUM geht das außerhalb der onCreate Methode nicht?
    //mDbHelper = new BookDbHelper(MainActivity.this);

    public void saveData(Sensor sensor) {

        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String strValues = "";
        for(float value : sensor.getValues()){
            strValues = strValues + value +"//";
        }


        ContentValues dbValues = new ContentValues();
        dbValues.put(SensorDbContract.SensorDbEntry.COLUMN_NAME_SENSOR_RATES, strValues );
        dbValues.put(SensorDbContract.SensorDbEntry.COLUMN_NAME_SENSOR_ID, sensor.getSensorType());
        dbValues.put(SensorDbContract.SensorDbEntry.COLUMN_NAME_TIMESTAMP, sensor.getTimestamp());
        dbValues.put(SensorDbContract.SensorDbEntry.COLUMN_NAME_ACCURACY, sensor.getAccuracy());
        db.insert(SensorDbContract.SensorDbEntry.TABLE_NAME, null, dbValues);

    }

    ArrayList<Sensor> loadSensor() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = null;
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder =
                SensorDbContract.SensorDbEntry._ID + " DESC";

        Cursor cursor = db.query(
                SensorDbContract.SensorDbEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );



        ArrayList<Sensor> result = new ArrayList<Sensor>();
        while (cursor.moveToNext()) {
            result.add(new Sensor(cursor.getInt(
                    cursor.getColumnIndexOrThrow(SensorDbContract.SensorDbEntry.COLUMN_NAME_SENSOR_ID)),
                            cursor.getString(cursor.getColumnIndexOrThrow(SensorDbContract.SensorDbEntry.COLUMN_NAME_SENSOR_RATES)),
                            cursor.getInt(cursor.getColumnIndexOrThrow(SensorDbContract.SensorDbEntry.COLUMN_NAME_ACCURACY)),
                            cursor.getLong(cursor.getColumnIndexOrThrow(SensorDbContract.SensorDbEntry.COLUMN_NAME_TIMESTAMP))
                    )
            );
        }

        cursor.close();

        return result;
    }

    public String loadSensorDB() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = null;
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder =
                SensorDbContract.SensorDbEntry._ID + " ASC";

        Cursor cursor = db.query(
                SensorDbContract.SensorDbEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");

        String result = "";
        while (cursor.moveToNext()) {
            result= result +
                    cursor.getString(cursor.getColumnIndexOrThrow(SensorDbContract.SensorDbEntry._ID)) +" Sensor: " +
                    cursor.getString(cursor.getColumnIndexOrThrow(SensorDbContract.SensorDbEntry.COLUMN_NAME_SENSOR_ID)) +" " +
                    cursor.getString(cursor.getColumnIndexOrThrow(SensorDbContract.SensorDbEntry.COLUMN_NAME_SENSOR_RATES))+" "+
                    cursor.getString(cursor.getColumnIndexOrThrow(SensorDbContract.SensorDbEntry.COLUMN_NAME_ACCURACY))+" "+
                    cursor.getString(cursor.getColumnIndexOrThrow(SensorDbContract.SensorDbEntry.COLUMN_NAME_TIMESTAMP))+"\n";

        }

        cursor.close();

        return result;
    }

    Set<Integer> getAllSensorIDs() {

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = null;
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder =
                SensorDbContract.SensorDbEntry._ID + " DESC";

        Cursor cursor = db.query(
                SensorDbContract.SensorDbEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );
        Set<Integer> idList = new HashSet<Integer>();

        while (cursor.moveToNext()) {
            idList.add(cursor.getInt(
                    cursor.getColumnIndexOrThrow(SensorDbContract.SensorDbEntry.COLUMN_NAME_SENSOR_ID))
            );


        }

        cursor.close();
        return idList;
    }


    public void deleteData(){

        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        db.execSQL(SQL_DELETE_ENTRIES);
        db.execSQL(SQL_CREATE_ENTRIES);

    }

    ArrayList<Datapoint> getSensorValues(int id, long startSession, long endSession){

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + SensorDbContract.SensorDbEntry.COLUMN_NAME_SENSOR_RATES +"," +
                SensorDbContract.SensorDbEntry.COLUMN_NAME_TIMESTAMP + " FROM " +
                SensorDbContract.SensorDbEntry.TABLE_NAME + " WHERE " + SensorDbContract.SensorDbEntry.COLUMN_NAME_SENSOR_ID
                + " = " + id + " AND " + SensorDbContract.SensorDbEntry.COLUMN_NAME_TIMESTAMP + " >= " + startSession + " AND " +
                SensorDbContract.SensorDbEntry.COLUMN_NAME_TIMESTAMP + "<= " + endSession

                , null);

        ArrayList<Datapoint> values = new ArrayList<Datapoint>();
        while(cursor.moveToNext()) {
            values.add(new Datapoint(cursor.getLong(cursor.getColumnIndexOrThrow(SensorDbContract.SensorDbEntry.COLUMN_NAME_TIMESTAMP))
                            , cursor.getString(cursor.getColumnIndexOrThrow(SensorDbContract.SensorDbEntry.COLUMN_NAME_SENSOR_RATES)),
                             id
                    )
            );
        }


        cursor.close();

        return values;

    }


    public void exportDB(Activity activity) {

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            Log.d(TAG, "Permission is not granted");

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Controller.REQUEST_PERMISSION);


        } else {


            File exportDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/BioReader");

            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }


            final String date = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.getDefault()).format(new Date());
            File file = new File(exportDir, "sensorDB_"+date+".csv");

            try {

                file.createNewFile();

                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                SQLiteDatabase db = mDbHelper.getReadableDatabase();
                Cursor curCSV = db.rawQuery("SELECT * FROM sensorDB", null);
                csvWrite.writeNext(curCSV.getColumnNames());



                while (curCSV.moveToNext()) {
                    //Which column you want to export
                    String arrStr[] = {curCSV.getString(0), curCSV.getString(1), curCSV.getString(2),curCSV.getString(3), curCSV.getString(4)};

                    csvWrite.writeNext(arrStr);
                }
                csvWrite.close();
                curCSV.close();
            } catch (Exception sqlEx) {

                Log.d(TAG, sqlEx.getMessage(), sqlEx);
            }

        }


    }


}

// Definition der Contract Klasse, in der gewisse Parameter wie Tabellenname festgelegt werden
// hier wird die Struktur der Datenbank aufgebaut
class SensorDbContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private SensorDbContract() {}

    /* Inner class that defines the table contents */
    public static class SensorDbEntry implements BaseColumns {
        public static final String TABLE_NAME = "sensorDB";
        public static final String COLUMN_NAME_SENSOR_ID = "sensorID";
        public static final String COLUMN_NAME_SENSOR_RATES = "sensorRates";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_ACCURACY = "accuracy";



    }
}

//Unterklasse die SQLiteOpenHelper extended. Dient der Organisation. Man erstellt
//ein Objekt der Klasse FeedReaderDBHelper und kann auf dieses Objekt die Methoden
// getWritableDatabase() or getReadableDatabase() aufrufen
class SensorDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Sensor.db";

    public SensorDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SensorDB.SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SensorDB.SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}