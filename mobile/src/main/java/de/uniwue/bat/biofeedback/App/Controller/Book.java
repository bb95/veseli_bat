package de.uniwue.bat.biofeedback.App.Controller;

import java.io.Serializable;
import java.sql.Array;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by BB on 05.03.2018.
 */

public class Book implements Serializable {


    private static final String TAG = Book.class.getSimpleName();
    //Meta Data
    public int bookID;
    public String bookUUID;
    public String author;
    public String title;

    //text as a whole; text separated in pages; one single page of all text
    public String text;
    public ArrayList<String> pages;
    public String page;

    //Pagenumber and Number of Pages
    public int pageNum;
    public int numPages;



    /**
     * This creates a book object that contains the author, title and whole text
     * as a String.
     * @param text represents the whole book as a String
     */
    public Book(String author, String title, String text) {
        this.author = author;
        this.title = title;
        this.text = text;

    }

    /**
     * This creates a book object that contains the author, title and also the text
     * separated in pages.
     * @param pages represents the whole book separated in pages as a ArrayList of Strings
     */
    public Book(String author, String title, ArrayList<String> pages) {
        this.author = author;
        this.title = title;
        this.pages = pages;

    }

    /**
     * This creates a book object that contains the number of a single page, the page itself
     * , the author and the title of the book the page belongs to and the number of all
     * pages in the book.
     * @param pageNum represents the Number of a single page
     * @param page represents a single page as a String
     * @param numPages represents the book's number of pages
     */
    public Book(int pageNum, String page, String author, String title, int numPages) {
        this.pageNum = pageNum;
        this.author = author;
        this.title = title;
        this.page = page;
        this.numPages = numPages;

    }

    public Book(int bookID, String bookUUID, String author, String title) {
        this.bookID = bookID;
        this.bookUUID = bookUUID;
        this.author = author;
        this.title = title;


    }

    public String getBookUUID() {
        return bookUUID;
    }



    @Override
    public String toString() {
        String result = "Book ID: " + bookUUID + "\n" +
                "Author: " + author + "\n" +
                "Title: " + title;

        return result;
    }

    public int getBookID() {
        return bookID;
    }

    public String getPage() {
        return page;
    }

    public int getNumPages() {
        return numPages;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<String> getPages() {
        return pages;
    }

    public int getPageNum() {
        return pageNum;
    }

    public String getText() {
        return text;
    }


    /**
     * This method separates a book into pages.
     * @param pageOffset sets the limit of a page. It represents
     *                   the number of characters according to that the text is
     *                   being separated.
     * @return is a ArrayList of the book's pages.
     */
    public ArrayList<String> getCharPerPage(int pageOffset){


        ArrayList<String> pages = new ArrayList<String>();
        int index = 0;
        int whitespacePos;
        while(index < text.length()) {

            if(index+ pageOffset >= text.length()) {
                whitespacePos = text.length();
            }else {
                whitespacePos =  getWhitespacePos(text, index+ pageOffset);
            }

            String page = text.substring(index, whitespacePos);
            pages.add(page);
            index = whitespacePos;


        }
        return pages;

    }

    public	ArrayList<String> listsToPages(ArrayList<List> wordlists){
        ArrayList<String> pages = new ArrayList<String>();
        for (List<String> wordlist : wordlists) {
            StringBuilder sb = new StringBuilder();
            for (String word : wordlist) {
                sb.append(word).append(" ");

            }
            String page = sb.toString().trim();
            pages.add(page);


        }

        return pages;
    }

    public ArrayList<String> splitText(int numOfWords, String divideby){

        List<String> splittedText = new ArrayList<String>();
        if(divideby=="sentence"){
            splittedText = splitTextIntoSentences(text);
        }
        else{
            splittedText = Arrays.asList(text.split(" "));
        }


        //List<String> splittedText = Arrays.asList(text.split(" "));

        ArrayList<List> wordlists = new ArrayList<List>();
        int rest = splittedText.size()%numOfWords;
        int size = splittedText.size()-rest;
        int start = 0;

        while(start < size) {

            List<String> wordlist = splittedText.subList(start, start+numOfWords);
            wordlists.add(wordlist);
            start = start+numOfWords;

        }
        List<String> lastWordlist= splittedText.subList(splittedText.size()-rest , splittedText.size());
        wordlists.add(lastWordlist);

        return listsToPages(wordlists);


    }

//    public ArrayList<String> getSentencesPerPage(int numOfSents){
//        List<String> splittedText = splitTextIntoSentences(text);
//
//
//        ArrayList<List> sentenceLists = new ArrayList<List>();
//        int rest = splittedText.size()%numOfSents;
//        int size = splittedText.size()-rest;
//        int start = 0;
//
//        while(start < size) {
//
//            List<String> wordlistsChunks = splittedText.subList(start, start+numOfSents);
//            sentenceLists.add(wordlistsChunks);
//            start = start+numOfSents;
//
//        }
//        List<String> lastPage= splittedText.subList(splittedText.size()-rest , splittedText.size());
//        sentenceLists.add(lastPage);
//        return listsToPages(sentenceLists);
//
//
//    }


    public ArrayList<String> splitTextIntoSentences(String text){

        String source = text;
        ArrayList<String> sentenceList = new ArrayList<String>();
        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.GERMANY);
        iterator.setText(source);
        int start = iterator.first();
        for (int end = iterator.next();
             end != BreakIterator.DONE;
             start = end, end = iterator.next()) {
            sentenceList.add(source.substring(start,end));
        }
        return sentenceList;
    }


    /**
     * This method returns the actual position, if it is a whitespace. Otherwise
     * it returns the position of a whitespace that is found first before the given position.
     *
     * @param text is the text in that the whitespaces is being searched
     * @param pos is the position that will be checked if it's a whitespace or not
     * @return is the position of a whitespace
     */
    private static int getWhitespacePos(String text, int pos) {

        for(int i = 0; i < pos; i++) {
            if(Character.isWhitespace(text.charAt(pos - i))) {
                return pos-i;
            }

        }
        return 0;

    }

}
