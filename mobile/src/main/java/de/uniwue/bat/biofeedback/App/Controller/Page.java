package de.uniwue.bat.biofeedback.App.Controller;

import java.io.Serializable;

/**
 * Created by BB on 18.03.2018.
 */

public class Page implements Serializable {

    public long timestamp; //timestamp can be the starting point or the ending point of a page
    public int pageNum;
    public int bookID;
    public String bookUUID;
    public long endTimestamp;


    /**
     * this creates a page objekt that contains the uuid of the related book, the pagenumber of the page and the start timestamp
     * @param bookUUID represents the uuid of the book, that is related to the page
     * @param pageNum represents the number of the page
     * @param timestamp represents the time, when the page first started
     */
    public Page(String bookUUID, int pageNum, long timestamp){
        this.bookUUID = bookUUID;
        this.pageNum = pageNum;
        this.timestamp = timestamp;
    }


    /**
     * this creates a page object that contains the uuid of the related book, the pagenumber of the page,
     * the start timestamp and the end timestamp
     * @param bookUUID represents the uuid of the book, that is related to the page
     * @param pageNum represents the number of the page
     * @param startTimestamp represents the time, when the page started
     * @param endTimestamp represents the time, when the page got finished
     */

    public Page(String bookUUID, int pageNum, long startTimestamp, long endTimestamp){
        this.bookUUID = bookUUID;
        this.pageNum = pageNum;
        this.timestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
    }

    @Override
    public String toString() {
        String result = "Book UUID: " + bookUUID+ "\n" +
                "Page: " + pageNum + "\n" +
                "Start Time: " + timestamp + "\n" +
                "End Time: " + endTimestamp + "\n" ;
        return result;

    }

    public String getBookUUID() {
        return bookUUID;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getPageNum() {
        return pageNum;
    }

    public int getBookID() {
        return bookID;
    }
}
