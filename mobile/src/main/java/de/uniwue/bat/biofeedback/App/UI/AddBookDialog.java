package de.uniwue.bat.biofeedback.App.UI;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import de.uniwue.bat.biofeedback.App.Controller.Book;
import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.R;

public class AddBookDialog extends AppCompatDialogFragment {

    private static final String TAG = AddBookDialog.class.getSimpleName();
    private EditText editAuthor;
    private EditText editTitle;
    private LibraryActivity listener;
    private String text;
    private ProgressDialog loadingText;
    private Controller controller = Controller.getInstance(getActivity());



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        text = (String) bundle.getSerializable(Controller.SELECTED_TEXT);


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_addbook, null);

        builder.setView(view)
                .setNegativeButton("Doch nicht!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton("Speichern", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        loadingText = new ProgressDialog(getActivity());
                        loadingText.setMessage("Loading Text");
                        loadingText.setCancelable(false);
                        loadingText.show();

                        String author = editAuthor.getText().toString();
                        String title = editTitle.getText().toString();

                        class TextTask extends AsyncTask<String, Integer, String> {
                            protected String doInBackground(String... args) {

                                Book selectedBook = new Book(args[0], args[1], text);
                                Log.d(TAG, "New Book "  );

                                controller.postMsg(Controller.NEW_BOOK, Controller.BOOK_TAG, selectedBook);
                                return null;
                            }

                            protected void onPostExecute(String result) {

                                controller.getControllerHandler().sendEmptyMessage(Controller.UPDATE_LIBRARY);

                                loadingText.cancel();

                            }
                        }

                        new TextTask().execute(new String[] {author,title} );

                    }
                });

        editAuthor = view.findViewById(R.id.edit_author);
        editTitle = view.findViewById(R.id.edit_title);

        return builder.create();


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        listener = (LibraryActivity) context;


    }

}
