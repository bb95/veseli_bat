package de.uniwue.bat.biofeedback.App.Databases;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.provider.BaseColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.ReadingSession;


public class VideoSessionsDB {

    private static final String TAG = StopReadingEventsDB.class.getSimpleName();
    private VideoSessionsDbHelper mDbHelper;


    public VideoSessionsDB(Context context){

        this.mDbHelper = new VideoSessionsDbHelper(context);
    }


    //Methode für den Create Table Befehl
    static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + VideoSessionsDbContract.VideoSessionsDbEntry.TABLE_NAME + " ( " +
                    VideoSessionsDbContract.VideoSessionsDbEntry._ID + " INTEGER PRIMARY KEY," +
                    VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_SESSION_UUID + " TEXT," +
                    VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_START_TIMESTAMP + " INTEGER," +
                    VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_END_TIMESTAMP + " INTEGER)";

    ;


    //Methode für den Delete Table Befehl
    static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + VideoSessionsDbContract.VideoSessionsDbEntry.TABLE_NAME;



    long saveData(long startMeasureTimestamp){
        //mDbHelper = new BookDbHelper(context);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues dbValues = new ContentValues();
        String uuid = UUID.randomUUID().toString();
        dbValues.put(VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_SESSION_UUID, uuid);
        dbValues.put(VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_START_TIMESTAMP, startMeasureTimestamp);
        dbValues.put(VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_END_TIMESTAMP, startMeasureTimestamp);


        long id = db.insert(VideoSessionsDbContract.VideoSessionsDbEntry.TABLE_NAME, null, dbValues);

        return id;
    }

    void saveEndingTimestamp(long stopMeasureTimestamp, long id){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String idAsString = id +"";
        String[] whereArgs = new String[] {idAsString };

        ContentValues dbValues = new ContentValues();
        dbValues.put(VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_END_TIMESTAMP, stopMeasureTimestamp);

        db.update(VideoSessionsDbContract.VideoSessionsDbEntry.TABLE_NAME,dbValues, VideoSessionsDbContract.VideoSessionsDbEntry._ID + " = ?", whereArgs);

        db.close();
    }


    ArrayList<ReadingSession> getAllReadingSession(String sessionUUID){

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] whereArgs = new String[] {sessionUUID};

        Cursor cursor = db.rawQuery("SELECT " + VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_START_TIMESTAMP +"," +
                VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_END_TIMESTAMP + " FROM " +
                VideoSessionsDbContract.VideoSessionsDbEntry.TABLE_NAME + " WHERE " + VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_SESSION_UUID
                + " = ?" , whereArgs);

        ArrayList<ReadingSession> readingSessions = new ArrayList<ReadingSession>();
        while(cursor.moveToNext()) {
            readingSessions.add(new ReadingSession(sessionUUID,cursor.getLong(cursor.getColumnIndexOrThrow(VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_START_TIMESTAMP))
                    , cursor.getLong(cursor.getColumnIndexOrThrow(VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_END_TIMESTAMP))));

        }


        cursor.close();

        return readingSessions;


    }


    public String loadData(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = null;
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder =
                VideoSessionsDbContract.VideoSessionsDbEntry._ID + " ASC";

        Cursor cursor = db.query(
                VideoSessionsDbContract.VideoSessionsDbEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        String result = "";
        while(cursor.moveToNext()) {
            result = result + cursor.getInt(
                    cursor.getColumnIndexOrThrow(VideoSessionsDbContract.VideoSessionsDbEntry._ID)) + " "
                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_SESSION_UUID)) + " "
                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_START_TIMESTAMP)) + " "
                    + cursor.getLong(
                    cursor.getColumnIndexOrThrow(VideoSessionsDbContract.VideoSessionsDbEntry.COLUMN_NAME_END_TIMESTAMP)) + " "
                    +"\n";

        }

        cursor.close();

        return result;
    }

    public void exportDB(Activity activity) {

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            Log.d(TAG, "Permission is not granted");

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Controller.REQUEST_PERMISSION);


        } else {


            File exportDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/BioReader");

            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }

            final String date = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.getDefault()).format(new Date());
            File file = new File(exportDir, "manualRecordedSessions_"+date+".csv");

            try {

                file.createNewFile();

                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                SQLiteDatabase db = mDbHelper.getReadableDatabase();
                Cursor curCSV = db.rawQuery("SELECT * FROM VideoSessionsDB", null);
                csvWrite.writeNext(curCSV.getColumnNames());



                while (curCSV.moveToNext()) {
                    //Which column you want to export
                    String arrStr[] = {curCSV.getString(0), curCSV.getString(1),curCSV.getString(2),curCSV.getString(3)};
                    csvWrite.writeNext(arrStr);
                }
                csvWrite.close();
                curCSV.close();
            } catch (Exception sqlEx) {

                Log.d(TAG, sqlEx.getMessage(), sqlEx);
            }
        }
    }

    // Definition der Contract Klasse, in der gewisse Parameter wie Tabellenname festgelegt werden
// hier wird die Struktur der Datenbank aufgebaut
    class VideoSessionsDbContract {
        // To prevent someone from accidentally instantiating the contract class,
        // make the constructor private.
        private VideoSessionsDbContract() {}

        /* Inner class that defines the table contents */
        public class VideoSessionsDbEntry implements BaseColumns {
            public static final String TABLE_NAME = "VideoSessionsDB";
            public static final String COLUMN_NAME_SESSION_UUID= "SessionUUID";
            public static final String COLUMN_NAME_START_TIMESTAMP= "TimeStampStart";
            public static final String COLUMN_NAME_END_TIMESTAMP = "TimeStampEnd";

        }
    }

    //Unterklasse die SQLiteOpenHelper extended. Dient der Organisation. Man erstellt
//ein Objekt der Klasse FeedReaderDBHelper und kann auf dieses Objekt die Methoden
// getWritableDatabase() or getReadableDatabase() aufrufen
    class VideoSessionsDbHelper extends SQLiteOpenHelper {
        // If you change the database schema, you must increment the database version.
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "VideoSession.db";

        public VideoSessionsDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
        }
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }
}