package de.uniwue.bat.biofeedback.App.Controller;

import java.io.Serializable;

/**
 * Created by BB on 19.07.2019.
 */

public class User implements Serializable {

    private static final String TAG = User.class.getSimpleName();
    public String username;
    public long registrationTime;

    public User(String username, long registrationTime) {
        this.username = username;
        this.registrationTime = registrationTime;


    }

    public String getUsername() {
        return username;
    }

    public long getRegistrationTime() {
        return registrationTime;
    }
}
