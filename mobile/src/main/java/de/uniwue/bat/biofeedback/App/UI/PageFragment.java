/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.uniwue.bat.biofeedback.App.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import de.uniwue.bat.biofeedback.App.Controller.Book;
import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.UI.LibraryActivity;
import de.uniwue.bat.biofeedback.R;



public class PageFragment extends Fragment {

    static final String TAG = PageFragment.class.getSimpleName();

    private int mPageNumber;
    private String author;
    private String title;
    private String page;
    private int numPages;
    private Book book;


    public static PageFragment create() {
        Log.d(TAG, "create()- erstellt Fragment");
        PageFragment fragment = new PageFragment();
        return fragment;
    }

    public PageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "Lifecycle - onCreate()");

        //long time= System.currentTimeMillis();
        //Log.d(TAG,"OnCreate: Time Value in milliseconds "+ time);

        if(savedInstanceState == null){


            book = (Book) getArguments().getSerializable(Controller.BOOK_DATA);
            Log.d(TAG, "Autor BOOK DATA" +book.getAuthor());

            Log.d(TAG, "ARGS_PAGE: " + mPageNumber);
        }

        else{

            //in onSaveInstantState wird das Buch gespeichert, welches ausgewählt wurde bzw. die TextID (getTExtID)
            // Diese ID brauche ich, um die Daten aus der DB auszulesen für das entsprechende Buch
            book =  (Book) savedInstanceState.getSerializable(Controller.SAVE_BOOK);


            Log.d(TAG, "ARGS_PAGE (ELSE): " + mPageNumber);
        }
        mPageNumber = book.getPageNum();
        author = book.getAuthor();
        title = book.getTitle();
        page = book.getPage();
        numPages = book.getNumPages();
        Log.d(TAG, "Book Data: " + mPageNumber+author+title+page);
        //numPages = book.getNumPages();

    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(TAG, "Lifecycle - onCreateView()");

        ViewGroup rootView;
        int seitenzahl = mPageNumber +1;


        if(mPageNumber == 0){

            rootView = (ViewGroup) inflater.inflate(R.layout.fragment_first_page, container, false);

            //Aufbau der Überschrift und Seitenanzahl
            ((TextView) rootView.findViewById(R.id.header_author)).setText(author);
            ((TextView) rootView.findViewById(R.id.header_title)).setText(title);



        }
        else if(mPageNumber == numPages-1) {

            rootView = (ViewGroup) inflater.inflate(R.layout.fragment_last_page, container, false);

            ((TextView) rootView.findViewById(R.id.heading_lastpage)).setText(author + ": " + title);

        }

        else{

            rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page, container, false);

            //Aufbau der Überschrift und Seitenanzahl
            ((TextView) rootView.findViewById(R.id.heading)).setText(author + ": " + title);



        }

        ((TextView) rootView.findViewById(R.id.pagenum)).setText(seitenzahl+"/"+numPages );
        ((TextView) rootView.findViewById(R.id.pageview)).setText(page);

        Log.d(TAG, "pageview update: " + mPageNumber);


        return rootView;


    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(Controller.SAVE_BOOK, book);
    }


    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }


    @Override
    public void onPause() {
        super.onPause();

        Log.d(TAG, "onPause()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
    }

    @Override
    public void onStop() {
        super.onStop();

        Log.d(TAG, "Lifecycle - onStop()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Log.d(TAG,"Lifecycle - onDestroyView()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "Lifecycle - onDestroy()");
    }

    @Override
    public void onDetach() {
        super.onDetach();

        Log.d(TAG, "Lifecycle - onDetach()");
    }


}
