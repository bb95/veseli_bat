package de.uniwue.bat.biofeedback.App.Controller;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by BB on 13.03.2018.
 */

public class Sensor implements Serializable {

    public int sensorType;
    public String strValues;
    public int accuracy;
    public long timestamp;
    public float[] values;


    /**
     * this creates a sensor object that contains the sensor id, the sensorvalues as a String, the accuracy and the timestamp
     * @param sensorType represents the id of the sensor
     * @param strValues represents the sensor values as String
     * @param accuracy represents the accuracy of the measured values
     * @param timestamp represents the timestamp when the values got measured
     */
    public Sensor(int sensorType, String strValues, int accuracy, long timestamp){
        this.sensorType = sensorType;
        this.strValues = strValues;
        this.accuracy = accuracy;
        this.timestamp = timestamp;
    }

    /**
     * this creates a sensor object that contains the sensor id, the sensorvalues as float array, the accuracy and the timestamp
     * @param sensorType represents the id of the sensor
     * @param values represents the sensor values as floats
     * @param accuracy represents the accuracy of the measured values
     * @param timestamp represents the timestamp when the values got measured
     */
    public Sensor(int sensorType, float[] values, int accuracy, long timestamp){
        this.sensorType = sensorType;
        this.values = values;
        this.accuracy = accuracy;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {

        String result = "Sensor ID: " + sensorType + "\n" +
                "Sensor Rate: " + strValues + "\n" +
                "Accuracy: " + accuracy + "\n" +
                "Timestamp: " + timestamp + "\n";

        return result;


    }

    public float[] getValues() {
        return values;
    }

    public int getSensorType() {
        return sensorType;
    }

    public String getStrValues() {
        return strValues;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public long getTimestamp() {
        return timestamp;
    }

}
