package de.uniwue.bat.biofeedback.App.UI;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;

import de.uniwue.bat.biofeedback.R;

/**
 * Created by BB on 12.04.2018.
 */

public class WearableHintDialog extends AppCompatDialogFragment {


    private static final String TAG = WearableHintDialog.class.getSimpleName();




    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_wearable_hint, null);

        builder.setView(view)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Intent libraryIntent = new Intent(getContext(), LibraryActivity.class);
                        startActivity(libraryIntent);



                    }
                });


        return builder.create();


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }




}
