package de.uniwue.bat.biofeedback.App.Databases;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.ArrayList;
import java.util.Set;

import de.uniwue.bat.biofeedback.App.Controller.Book;
import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.Datapoint;
import de.uniwue.bat.biofeedback.App.Controller.Event;
import de.uniwue.bat.biofeedback.App.Controller.BookDbEntry;
import de.uniwue.bat.biofeedback.App.Controller.Page;
import de.uniwue.bat.biofeedback.App.Controller.ReadingSession;
import de.uniwue.bat.biofeedback.App.Controller.Sensor;
import de.uniwue.bat.biofeedback.App.Controller.PagesDbEntry;
import de.uniwue.bat.biofeedback.App.Controller.User;

/**
 * Created by BB on 04.03.2018.
 */

public class DBInterface {

    private final String TAG = DBInterface.class.getSimpleName();
    private BookDB metaDB;
    private Context context;
    private PagesDB textDB;
    private SensorDB sensorDB;
    private StopReadingEventsDB stopReadingEventDB;
    private PageSessionsDB readingProgressDB;
    private UserDB userDB;
    private long progessDBid;
    private long userID;
    private BookSessionsDB bookSessionDB;
    private long bookSessionID;
    private VideoSessionsDB measurementSessionDB;
    private long measurementSessionID;


    public DBInterface(Context context) {
        this.context = context;
        this.metaDB = new BookDB(context);
        this.textDB = new PagesDB(context);
        this.sensorDB = new SensorDB(context);
        this.readingProgressDB = new PageSessionsDB(context);
        this.stopReadingEventDB = new StopReadingEventsDB(context);
        this.bookSessionDB = new BookSessionsDB(context);
        this.measurementSessionDB = new VideoSessionsDB(context);
        this.userDB = new UserDB(context);

        Controller.getInstance(context).addObserver(dbInterfaceHandler);
    }

    private void saveTextData(PagesDbEntry entry) {

        textDB.saveData(entry);


    }


    private String saveMetaData(BookDbEntry entry) {

        return metaDB.saveData(entry);


    }

    private ArrayList<Sensor> loadSensorData() {

        return sensorDB.loadSensor();

    }


    private ArrayList<Book> getRowsFromMetaDB() {
        return metaDB.getRowsFromDB();
    }

    private String getAuthor(String uuid) {
        return metaDB.getAuthorByID(uuid);
    }

    private String getTitle(String uuid) {
        return metaDB.getTitleByID(uuid);
    }

    private ArrayList<String> getPagesFromDB(String uuid) {
        return textDB.getTextByID(uuid);
    }


    private long saveUserData(String username, long timestamp){
        return userDB.saveData(username,timestamp);
    }

    private void saveLogoutTimestamp(long timestampEnd, long rowID) {
        userDB.saveEndingTimestamp(timestampEnd, rowID);
    }


    private long saveReadingProgess(String bookUUID, int pageNum, long timestamp) {
        return readingProgressDB.saveData(bookUUID, pageNum, timestamp);
    }

    private void saveEndingTimestamp(String bookUUID, int pageNum, long timestamp, long rowID) {
        readingProgressDB.saveEndingTimestamp(bookUUID, pageNum, timestamp, rowID);
    }

    private String loadReadingProgress() {
        return readingProgressDB.loadData();
    }

    private ArrayList<Page> loadReadingProgressAsList() {
        return readingProgressDB.loadDataAsArrayList();
    }

    private void deleteBookFromMetaDataDB(String uuid) {
        metaDB.deleteData(uuid);
    }

    private void deleteBookFromReadingProgressDB(String uuid) {
        readingProgressDB.deleteData(uuid);
    }

    private void deleteBookFromTextDataDB(String uuid) {
        textDB.deleteData(uuid);
    }

    private void saveEvent(Event event) {
        stopReadingEventDB.saveData(event);
    }

    private String loadEvents() {
        return stopReadingEventDB.loadData();
    }

    private int getPageByTime(long timestamp) {
        return readingProgressDB.getPagesByTimestamp(timestamp);
    }

    private Set<Integer> getAllSensorIDs() {

        return sensorDB.getAllSensorIDs();

    }


    private ArrayList<Datapoint> getSensorDataByID(int sensorID, long startSession, long endSession) {
        return sensorDB.getSensorValues(sensorID, startSession, endSession);
    }

    private long saveStartBookEvent(Event startBookEvent) {
        return bookSessionDB.saveData(startBookEvent);
    }

    private void saveEndBookEvent(Event endBookEvent, long bookSessionID) {
        bookSessionDB.saveEndingTimestamp(endBookEvent, bookSessionID);
    }

    private ArrayList<ReadingSession> getReadingSessions(String bookUUID) {
        return bookSessionDB.getAllReadingSession(bookUUID);
    }

    private long saveMeasureSession(long startMeasureTimestamp) {
        return measurementSessionDB.saveData(startMeasureTimestamp);
    }

    private void saveMeasurementSessionEnd(long endMeasureTimestamp, long id) {
        measurementSessionDB.saveEndingTimestamp(endMeasureTimestamp, id);
    }

    private void saveSensorData(Sensor sensor) {
        sensorDB.saveData(sensor);
    }


    Handler dbInterfaceHandler = new Handler() {


        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {

                case Controller.NEW_BOOK:


                    Log.d(TAG, "NewBook");
                    Bundle newBookBundle = msg.getData();
                    Book book = (Book) newBookBundle.getSerializable(Controller.BOOK_TAG);



                    BookDbEntry metaData = new BookDbEntry(book.getAuthor(), book.getTitle());
                    String bookUUID = saveMetaData(metaData);
                    Log.d(TAG, "NEW Book UUID " + bookUUID);

                    int pagenum = 1;
                    for (String page : book.getCharPerPage(1000)) {

                        saveTextData(new PagesDbEntry(bookUUID, page, pagenum));
                        Log.d(TAG, "TEXT DB UUID " + bookUUID);
                        Log.d(TAG, "PartPage \n" + page);

                        pagenum++;
                    }
                    Log.d(TAG, "TEXT DB EINTRAG " + textDB.loadData());
                    Log.d(TAG, "Meta DB EINTRAG " + metaDB.loadData());

                    break;


                case Controller.UPDATE_LIBRARY:

                    Log.d(TAG, "ROWS FROM DB " + getRowsFromMetaDB().toString());
                    Controller.getInstance(context).postMsg(Controller.LIBRARY_CONTENT, Controller.LIBRARY_TAG, getRowsFromMetaDB());

                    break;

                case Controller.NEW_USER:

                    Bundle userBundle = msg.getData();
                    User user = (User) userBundle.getSerializable(Controller.USER_TAG);
                    String username = user.getUsername();
                    long timestampStart = user.getRegistrationTime();

                    Log.d(TAG, "Username "+ username+" Timestamp "+timestampStart);
                    userID = saveUserData(username,timestampStart);



                    break;

                case Controller.LOGOUT_USER:

                    Bundle logoutBundle = msg.getData();
                    long logoutTimestamp = (long) logoutBundle.getSerializable(Controller.LOGOUT_USER_TAG);

                    Log.d(TAG, "Userid "+ userID+" LogoutTimestamp "+logoutTimestamp);
                    saveLogoutTimestamp(logoutTimestamp,userID);




                    break;




                case Controller.GET_BOOK_DATA:

                    Bundle dataBundle = msg.getData();
                    String listUUID = (String) dataBundle.getSerializable(Controller.GET_BOOK_DATA_TAG);
                    Log.d(TAG, "listUUID " + listUUID);

                    Book bookData = new Book(getAuthor(listUUID), getTitle(listUUID), getPagesFromDB(listUUID));

                    Log.d(TAG, "Author UUID" + getAuthor(listUUID) + " " + getTitle(listUUID));

                    Log.d(TAG, "getPagesFromDB with ID: " + getPagesFromDB(listUUID));

                    Controller.getInstance(context).postMsg(Controller.SEND_BOOK_DATA, Controller.SEND_BOOK_DATA_TAG, bookData);
                    break;

                case Controller.GET_SENSOR_DATA:

                    ArrayList<Sensor> sensorData = loadSensorData();

                    Controller.getInstance(context).postMsg(Controller.SEND_SENSOR_DATA, Controller.SEND_SENSOR_DATA_TAG, sensorData);

                    break;

                case Controller.START_PAGE:

                    Bundle startTimeBundle = msg.getData();
                    Page page = (Page) startTimeBundle.getSerializable(Controller.START_PAGE_TAG);
                    Log.d(TAG, "Page Data " + page.getBookUUID() + " " + page.getPageNum() + " " + page.getTimestamp());
                    progessDBid = saveReadingProgess(page.getBookUUID(), page.getPageNum(), page.getTimestamp());
                    Log.d(TAG, "Reading Progress: " + "\n" + loadReadingProgress());
                    Log.d(TAG, "Reading Progress DB ID " + progessDBid);

                    break;

                case Controller.END_PAGE:
                    Bundle endTimeBundle = msg.getData();
                    Page page1 = (Page) endTimeBundle.getSerializable(Controller.END_PAGE_TAG);
                    saveEndingTimestamp(page1.getBookUUID(), page1.getPageNum(), page1.getTimestamp(), progessDBid);

                    break;

                case Controller.GET_READING_PROGRESS_DATA:
                    ArrayList<Page> readingProgressData = loadReadingProgressAsList();

                    Controller.getInstance(context).postMsg(Controller.SEND_READING_PROGRESS_DATA, Controller.SEND_READING_PROGRESS_TAG, readingProgressData);

                    break;

                case Controller.DELETE_BOOK:

                    Bundle deleteBookIDBndl = msg.getData();
                    String delId = (String) deleteBookIDBndl.getSerializable(Controller.DELETE_BOOK_TAG);

                    deleteBookFromMetaDataDB(delId);
                    deleteBookFromReadingProgressDB(delId);
                    deleteBookFromTextDataDB(delId);

                    break;

                case Controller.SESSION_DATA:



                    Bundle sessionTimespanBndl = msg.getData();
                    ReadingSession readingSession = (ReadingSession) sessionTimespanBndl.getSerializable(Controller.SESSION_DATA_TAG);

                    long startTime = readingSession.getStartTime();
                    long endTime = readingSession.getEndTime();
                    Log.d(TAG, "ReadingSession: " + readingSession.getBookUUID() + " " + startTime + " " + endTime);
                    Log.d(TAG, "AllSensorIDs: " + getAllSensorIDs());

                    ArrayList<ArrayList<Datapoint>> sensorGraphList = new ArrayList<ArrayList<Datapoint>>();

                    for (int i : getAllSensorIDs()) {

                        ArrayList<Datapoint> sensorGraph = getSensorDataByID(i, startTime, endTime);

                        for (Datapoint datapoint : sensorGraph) {
                            datapoint.setPage(getPageByTime(datapoint.getX()));
                        }
                        //Log.d(TAG, "SensorGraph yValues"+sensorGraph.get(0).getId() +" "+sensorGraph.get(0).getY());
                        sensorGraphList.add(sensorGraph);
                    }



                    Controller.getInstance(context).postMsg(Controller.SEND_SESSION_DATA, Controller.SEND_SESSION_DATA_TAG, sensorGraphList);


                    break;

                case Controller.SEND_START_BOOK_EVENT:
                    Bundle bookEventBndl = msg.getData();
                    Event startBookEvent = (Event) bookEventBndl.getSerializable(Controller.SEND_START_BOOK_EVENT_TAG);
                    bookSessionID = saveStartBookEvent(startBookEvent);

                    Log.d(TAG, "StartSession: Book " + startBookEvent.getEventName() + " Time " + startBookEvent.getTimestamp());

                    break;

                case Controller.SEND_OFF_SCREEN_EVENT:
                    Bundle offScreenEventBndl = msg.getData();
                    Event offScreenEvent = (Event) offScreenEventBndl.getSerializable(Controller.SEND_OFF_SCREEN_EVENT_TAG);

                    saveEndBookEvent(offScreenEvent, bookSessionID);

                    saveEvent(offScreenEvent);

                    Log.d(TAG, "EndSession OffScreen: " + " Time " + offScreenEvent.getTimestamp());


                    Log.d(TAG, "OffScreen Event: " + loadEvents());

                    break;

                case Controller.SEND_HOME_TAB_EVENT:
                    Bundle tabOrHomeEventBndl = msg.getData();
                    Event tabOrHomeEvent = (Event) tabOrHomeEventBndl.getSerializable(Controller.SEND_HOME_TAB_EVENT_TAG);

                    saveEndBookEvent(tabOrHomeEvent, bookSessionID);

                    saveEvent(tabOrHomeEvent);

                    Log.d(TAG, "EndSession: HomeTab " + " Time " + tabOrHomeEvent.getTimestamp());


                    Log.d(TAG, "HomeTab Event: " + loadEvents());

                    break;

                case Controller.SEND_BACKPRESS_EVENT:
                    Bundle backpressEventBndl = msg.getData();
                    Event backpressEvent = (Event) backpressEventBndl.getSerializable(Controller.SEND_BACKPRESS_EVENT_TAG);

                    saveEndBookEvent(backpressEvent, bookSessionID);

                    saveEvent(backpressEvent);

                    Log.d(TAG, "BackPress Event: " + loadEvents());
                    Log.d(TAG, "EndSession: Backpress " + " Time " + backpressEvent.getTimestamp());


                    break;

                case Controller.GET_SESSIONS:
                    Bundle bookUuidBndl = msg.getData();
                    String sessionBook = (String) bookUuidBndl.getSerializable(Controller.GET_SESSIONS_TAG);


                    Controller.getInstance(context).postMsg(Controller.SEND_SESSIONS, Controller.SEND_SESSIONS_TAG, getReadingSessions(sessionBook));

                    break;

                case Controller.START_MEASUREMENT:

                    Bundle startMeasureBndl = msg.getData();
                    long startMeasureTimestamp = startMeasureBndl.getLong(Controller.START_MEASUREMENT_TAG);

                    measurementSessionID = saveMeasureSession(startMeasureTimestamp);

                    break;
                case Controller.STOP_MEASUREMENT:

                    Bundle stopMeasureBndl = msg.getData();
                    long stopMeasureTimestamp = stopMeasureBndl.getLong(Controller.STOP_MEASUREMENT_TAG);
                    saveMeasurementSessionEnd(stopMeasureTimestamp, measurementSessionID);
                    break;

                case Controller.SAVE_SENSOR_DATA:
                    Bundle saveSensorBndl = msg.getData();
                    Sensor saveSensor = (Sensor) saveSensorBndl.getSerializable(Controller.SAVE_SENSOR_DATA_TAG);
                    Log.d(TAG, "saved sensor data: "+ saveSensor.getSensorType());
                    saveSensorData(saveSensor);

                    break;


            }


        }
    };


}
