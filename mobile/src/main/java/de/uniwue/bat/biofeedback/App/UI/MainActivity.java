package de.uniwue.bat.biofeedback.App.UI;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.User;
import de.uniwue.bat.biofeedback.App.UI.BiofeedbackVisualization.VisualizationLibraryActivity;
import de.uniwue.bat.biofeedback.R;



public class MainActivity extends AppCompatActivity{

    private static final String TAG = MainActivity.class.getSimpleName();
    private Button libraryBtn;
    private Button measurementBtn;
    private TextView bioReader;
    private Controller controller;
    private Button  realVisualizationBtn;
    Toolbar mToolbar;

    private SharedPreferences.Editor editor;
    private SharedPreferences settings;

    //SharedPreferences Keys
    private final String KEY_USERNAME = "username";
    private final String KEY_LOGGED_IN = "isLoggedIn";
    private final String KEY_SHARED_PREF = "SharedPref";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "Lifecycle - onCreate");


        settings = getSharedPreferences(KEY_SHARED_PREF, MODE_PRIVATE);
        editor = settings.edit();
        // Reading from SharedPreferences
        String savedUsername = settings.getString(KEY_USERNAME, "");
        boolean savedLoginStatus = settings.getBoolean(KEY_LOGGED_IN, false);
        //check Preferences
        Log.d(TAG,"SharedPref "+ savedUsername+" "+ savedLoginStatus);


        controller = Controller.getInstance(this);
        bioReader = (TextView) findViewById(R.id.bioReader_text);
        bioReader.setText("BioReader");
        libraryBtn = (Button) findViewById(R.id.eReader_btn);
        realVisualizationBtn = (Button) findViewById(R.id.realVisualizationBtn);
        measurementBtn = (Button) findViewById(R.id.wearableManipulation);
        mToolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        initToolbar();


        if(!savedLoginStatus){
            showUserDialog(MainActivity.this);
        }


        realVisualizationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent realVisualizationIntent = new Intent(MainActivity.this, VisualizationLibraryActivity.class);
                startActivity(realVisualizationIntent);

            }
        });


        libraryBtn.setOnClickListener( new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Library Button got clicked");
                openDialog();
            }
        });


        measurementBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent readingProgressIntent = new Intent(MainActivity.this, ManualRecorderActivity.class);
                startActivity(readingProgressIntent);

            }
        });


        }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem actionLogout = menu.findItem(R.id.action_logout);
        MenuItem actionLogin = menu.findItem(R.id.action_login);
        if (settings.getBoolean(KEY_LOGGED_IN, false)) {

            actionLogout.setVisible(true);
            actionLogout.setTitle("Abmelden: "+settings.getString(KEY_USERNAME, ""));
        }
        else{
            actionLogin.setVisible(true);
        }
        if (!actionLogout.isVisible()){
            actionLogin.setVisible(true);
            actionLogin.setTitle("Anmelden");
        }




        return super.onCreateOptionsMenu(menu);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(false);
            ab.setTitle("");
            mToolbar.setNavigationIcon(R.drawable.buch_lese_logo_x);
            mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_export:
                            startActivity(new Intent(MainActivity.this, ExportActivity.class));
                            return true;
                        case R.id.action_logout:

                            long logoutTime = System.currentTimeMillis();
                            controller.postMsg(Controller.LOGOUT_USER, Controller.LOGOUT_USER_TAG, logoutTime);
                            item.setVisible(false);
                            editor.clear();
                            editor.commit();

                            //check Preferences after Logout
                            Log.d(TAG, "SharedPref after logout "+settings.getString(KEY_USERNAME, "")+ " "+ settings.getBoolean(KEY_LOGGED_IN, false));

                            initToolbar();
                            return true;
                        case R.id.action_login:

                            showUserDialog(MainActivity.this);
                            item.setVisible(false);

                            return true;


                    }

                    return true;
                }
            });
        }
    }


    public void showUserDialog(Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.dialog_register_user, null);
        final EditText editUsername;
        editUsername = view.findViewById(R.id.edit_username);

        builder.setView(view)
                .setPositiveButton("Speichern", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {



                        String username = editUsername.getText().toString();
                        long registrationTime = System.currentTimeMillis();
                        User user = new User(username,registrationTime );
                        controller.postMsg(Controller.NEW_USER, Controller.USER_TAG, user);

                        editor.putString(KEY_USERNAME, username);
                        editor.putBoolean(KEY_LOGGED_IN, true);
                        editor.commit();

                        // Check Preferences after Login
                        Log.d(TAG, "SharedPref after login "+settings.getString(KEY_USERNAME, "")+ " "+ settings.getBoolean(KEY_LOGGED_IN, false));
                        initToolbar();



                    }
                });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                initToolbar();
            }
        });


        AlertDialog alert = builder.create();
        alert.show();


    }

    public void openDialog(){

        WearableHintDialog wearableHintDialog = new WearableHintDialog();
        wearableHintDialog.show(getSupportFragmentManager(), Controller.WEARABLE_HINT_DIALOG);
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "Lifecycle onDestroy");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Lifecycle onDestroy");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Lifecycle onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "Lifecycle onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Lifecycle onResume");
        //initToolbar();
    }
}
