package de.uniwue.bat.biofeedback.App.Databases;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.provider.BaseColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import de.uniwue.bat.biofeedback.App.Controller.Book;
import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.BookDbEntry;


public class BookDB {

    private static final String TAG = BookDB.class.getSimpleName();
    private BookDbHelper mDbHelper;


    public BookDB(Context context){

        this.mDbHelper = new BookDbHelper(context);
    }


    //Methode für den Create Table Befehl
    static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + BookDbContract.BookDbEntry.TABLE_NAME + " ( " +
                    BookDbContract.BookDbEntry._ID + " INTEGER PRIMARY KEY," +
                    BookDbContract.BookDbEntry.COLUMN_NAME_BOOK_UUID + " TEXT," +
                    BookDbContract.BookDbEntry.COLUMN_NAME_AUTHOR + " TEXT," +
                    BookDbContract.BookDbEntry.COLUMN_NAME_TITLE + " TEXT)";

    //Methode für den Delete Table Befehl
    static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + BookDbContract.BookDbEntry.TABLE_NAME;



    public String saveData(BookDbEntry entry){
        //mDbHelper = new BookDbHelper(context);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues dbValues = new ContentValues();
        dbValues.put(BookDbContract.BookDbEntry.COLUMN_NAME_AUTHOR, entry.author);
        dbValues.put(BookDbContract.BookDbEntry.COLUMN_NAME_TITLE, entry.title);
        String uuid = UUID.randomUUID().toString();
        dbValues.put(BookDbContract.BookDbEntry.COLUMN_NAME_BOOK_UUID, uuid);


        db.insert(BookDbContract.BookDbEntry.TABLE_NAME, null, dbValues);

        return uuid;
    }

    ArrayList<Book> getRowsFromDB(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = {
                BookDbContract.BookDbEntry._ID,
                BookDbContract.BookDbEntry.COLUMN_NAME_BOOK_UUID,
                BookDbContract.BookDbEntry.COLUMN_NAME_AUTHOR,
                BookDbContract.BookDbEntry.COLUMN_NAME_TITLE
        };
        String selection =  null;
        String[] selectionArgs = null;
        String sortOrder =
                BookDbContract.BookDbEntry._ID + " DESC";

        Cursor cursor = db.query(
                BookDbContract.BookDbEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );




        ArrayList<Book> result = new ArrayList<Book>();
        while(cursor.moveToNext()) {
            result.add(new Book(cursor.getInt(cursor.getColumnIndexOrThrow(BookDbContract.BookDbEntry._ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(BookDbContract.BookDbEntry.COLUMN_NAME_BOOK_UUID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(BookDbContract.BookDbEntry.COLUMN_NAME_AUTHOR)),
                    cursor.getString(cursor.getColumnIndexOrThrow(BookDbContract.BookDbEntry.COLUMN_NAME_TITLE))
                )
            );

        }




        cursor.close();



        return result;

    }

    public String loadData(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = null;
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder =
                BookDbContract.BookDbEntry._ID + " ASC";

        Cursor cursor = db.query(
                BookDbContract.BookDbEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        String result = "";
        while(cursor.moveToNext()) {
            result = result + cursor.getInt(
                    cursor.getColumnIndexOrThrow(BookDbContract.BookDbEntry._ID)) + " "

                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(BookDbContract.BookDbEntry.COLUMN_NAME_BOOK_UUID)) + " "
                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(BookDbContract.BookDbEntry.COLUMN_NAME_AUTHOR)) + " "
                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(BookDbContract.BookDbEntry.COLUMN_NAME_TITLE))
                    +"\n";

        }

        cursor.close();

        return result;
    }

    public String getAuthorByID(String uuid){

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        //String idAsString = id +"";

        String[] projection = {
                BookDbContract.BookDbEntry._ID,
                BookDbContract.BookDbEntry.COLUMN_NAME_BOOK_UUID,
                BookDbContract.BookDbEntry.COLUMN_NAME_AUTHOR,
        };
        String selection =  BookDbContract.BookDbEntry.COLUMN_NAME_BOOK_UUID + " = ?";
        String[] selectionArgs = { uuid };
        String sortOrder =
                BookDbContract.BookDbEntry._ID + " DESC";

        Cursor cursor = db.query(
                BookDbContract.BookDbEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        String result = "";
        while(cursor.moveToNext()) {
            result = result + cursor.getString(
                    cursor.getColumnIndexOrThrow(BookDbContract.BookDbEntry.COLUMN_NAME_AUTHOR));

        }

        cursor.close();

        return result;
    }

    public String getTitleByID(String uuid){

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        //String idAsString = id +"";


        String[] projection = {
                BookDbContract.BookDbEntry._ID,
                BookDbContract.BookDbEntry.COLUMN_NAME_BOOK_UUID,
                BookDbContract.BookDbEntry.COLUMN_NAME_TITLE,
        };
        String selection =  BookDbContract.BookDbEntry.COLUMN_NAME_BOOK_UUID + " = ?";
        String[] selectionArgs = { uuid };
        String sortOrder =
                BookDbContract.BookDbEntry._ID + " DESC";

        Cursor cursor = db.query(
                BookDbContract.BookDbEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        String result = "";
        while(cursor.moveToNext()) {
            result = result + cursor.getString(
                    cursor.getColumnIndexOrThrow(BookDbContract.BookDbEntry.COLUMN_NAME_TITLE));

        }

        cursor.close();

        return result;
    }

    public String getRowByID(long id){
        //mDbHelper = new BookDbHelper(context);
        String idAsString = id +"";
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = {
                BookDbContract.BookDbEntry._ID,
                BookDbContract.BookDbEntry.COLUMN_NAME_AUTHOR,
                BookDbContract.BookDbEntry.COLUMN_NAME_TITLE
        };
        String selection =  BookDbContract.BookDbEntry._ID + " = ?";
        String[] selectionArgs = { idAsString };
        String sortOrder =
                BookDbContract.BookDbEntry._ID + " DESC";

        Cursor cursor = db.query(
                BookDbContract.BookDbEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        String result = "";
        while(cursor.moveToNext()) {
            result = result + cursor.getString(
                    cursor.getColumnIndexOrThrow(BookDbContract.BookDbEntry.COLUMN_NAME_AUTHOR)) + ": "
                    + cursor.getString(
                    cursor.getColumnIndexOrThrow(BookDbContract.BookDbEntry.COLUMN_NAME_TITLE)) + " "
                    +"\n";

        }

        cursor.close();

        return result;
    }

    public void deleteData(String uuid){

        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String[] whereArgs = new String[] {uuid};


        db.delete(BookDbContract.BookDbEntry.TABLE_NAME, BookDbContract.BookDbEntry.COLUMN_NAME_BOOK_UUID + " = ?", whereArgs);
        db.close();

    }

    public void exportDB(Activity activity) {

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            Log.d(TAG, "Permission is not granted");

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Controller.REQUEST_PERMISSION);


        } else {


            File exportDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/BioReader");

            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }


            final String date = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.getDefault()).format(new Date());
            File file = new File(exportDir, "bookDB_"+date+".csv");

            Log.d(TAG, "Datum und Uhrzeit: "+date);
            try {

                file.createNewFile();

                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                SQLiteDatabase db = mDbHelper.getReadableDatabase();
                Cursor curCSV = db.rawQuery("SELECT * FROM BookDB", null);
                csvWrite.writeNext(curCSV.getColumnNames());



                while (curCSV.moveToNext()) {
                    //Which column you want to export
                    String arrStr[] = {curCSV.getString(0), curCSV.getString(1), curCSV.getString(2), curCSV.getString(3)};
                    csvWrite.writeNext(arrStr);
                }
                csvWrite.close();
                curCSV.close();
            } catch (Exception sqlEx) {

                Log.d(TAG, sqlEx.getMessage(), sqlEx);
            }
        }
    }


// Definition der Contract Klasse, in der gewisse Parameter wie Tabellenname festgelegt werden
// hier wird die Struktur der Datenbank aufgebaut
class BookDbContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private BookDbContract() {}

    /* Inner class that defines the table contents */
    public class BookDbEntry implements BaseColumns {
        public static final String TABLE_NAME = "BookDB";
        public static final String COLUMN_NAME_AUTHOR = "Author";
        public static final String COLUMN_NAME_TITLE = "Title";
        public static final String COLUMN_NAME_BOOK_UUID = "BookUUID";




    }
}

//Unterklasse die SQLiteOpenHelper extended. Dient der Organisation. Man erstellt
//ein Objekt der Klasse FeedReaderDBHelper und kann auf dieses Objekt die Methoden
// getWritableDatabase() or getReadableDatabase() aufrufen
class BookDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Book.db";

    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
}