package de.uniwue.bat.biofeedback.App.Functions;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.util.Log;

import de.uniwue.bat.biofeedback.App.Controller.Controller;
import de.uniwue.bat.biofeedback.App.Controller.Event;

import static android.content.ContentValues.TAG;

/**
 * Created by BB on 04.04.2018.
 */

public class ScreenReceiver extends BroadcastReceiver {

    public static boolean wasScreenOn = true;



    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {

            wasScreenOn = false;
            Log.d(TAG, "OffScreen ");

            long timestamp = System.currentTimeMillis();
            Event event = new Event("ScreenOff", timestamp);
            Controller.getInstance(context).postMsg(Controller.SEND_OFF_SCREEN_EVENT, Controller.SEND_OFF_SCREEN_EVENT_TAG, event );

        }



    }

}
