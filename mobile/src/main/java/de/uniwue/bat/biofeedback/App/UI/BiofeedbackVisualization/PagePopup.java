package de.uniwue.bat.biofeedback.App.UI.BiofeedbackVisualization;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

import de.uniwue.bat.biofeedback.R;

/**
 * Created by BB on 09.04.2018.
 */

public class PagePopup extends MarkerView {

    private TextView tvContent;
    private final String TAG = PagePopup.class.getSimpleName();

    public PagePopup(Context context, int layoutResource) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.tvContent);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        //ArrayList<MyData> sensorDataPairs = sensorDB.getSensorDataByID(21);
        //Log.d(TAG,"PageByTimestamp "+  readingProgressDB.getPagesByTimestamp(sensorDataPairs.get(0).getxValAsLong()));

        Log.d(TAG,"MarkerView: refresh Content - x: "+ e.getX() + " y: "+ e.getY() );

        if((int) e.getData()==0){
            tvContent.setText("Umgeblättert");
        }
        else{
            tvContent.setText("S. " + e.getData());
        }

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {

        return new MPPointF(-(getWidth() / 2), -getHeight());

    }



}


